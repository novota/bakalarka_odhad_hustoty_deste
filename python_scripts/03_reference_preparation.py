
import datetime
import numpy as np
import pandas as pd
import pathlib


def load_rg_calculate_reference(path_to_rain_gauges_data: pathlib.Path, resample_time: str, time_res: int, low_thres: float,
		high_thres: float, output_path_pkl: pathlib.Path, output_path_csv: pathlib.Path):
	"""Get rain gauges data, resample them to resample_time resolution that should math the resolution of cml data.
		Create reference from each rain gauge. Reference is calculated from a maximum of a time_res time window in such a way that
		it rains if maximum is bigger then high_thres (1), it does not rain (2) if maximum is below low_thres and else (-1) we dont know.

	:param path_to_rain_gauges_data: path to rain gauge data
	:param resample_time: time step to which data is resampled
	:param time_res: reference window
	:param low_thres: below this threshold reference says it does not rain
	:param high_thres: above this threshold reference says it does rain
	:param output_path_pkl: output path where result.pkl is saved
	:param output_path_csv: output path where result.csv is saved
	:return: None
	"""
	df = pd.read_csv(path_to_rain_gauges_data, delimiter=";", low_memory=False)

	# create date time from timestamp
	df.time = [datetime.datetime.fromtimestamp(x) for x in df.time]
	# set time as index
	df = df.set_index("time")
	# deal with Nan values
	df.replace("   NA", np.nan, inplace=True)
	# convert datatypes to numeric
	df = df.apply(pd.to_numeric)
	# aggregate duplicate indexes
	df = df.groupby(df.index).mean()
	# resample to data resolution
	df = df.resample(resample_time).mean()
	# compute average of all rain gauges
	df["average_rain_gauges"] = df.loc[:, :].mean(axis=1)

	# compute max in rolling window
	d = df.rolling(time_res, min_periods=time_res // 2).max()
	rain_gauges = ["D10", "D13", "D22"]

	# create reference for each rain gauge
	for rg in rain_gauges:
		# apply rules
		d.loc[d[rg] >= high_thres, f"{rg}_GT"] = 2
		d.loc[d[rg] < low_thres, f"{rg}_GT"] = 1
		d.loc[(d[rg] < high_thres) & (d[rg] >= low_thres), f"{rg}_GT"] = 0
		d.loc[d[rg].isna(), f"{rg}_GT"] = 0

		# append as new column to existing data frame
		df = df.join(d[f"{rg}_GT"], how="outer")

	# create aggregated reference
	# it rains only if all rain gauges values are above high_thres
	df["GT_aggr"] = df["D10_GT"] * df["D13_GT"] * df["D22_GT"]
	# if it rains everywhere, result is 6, if at least one rain gauge is not sure, reuslt is 0
	df.loc[df["GT_aggr"] == 0, "GT_aggr"] = -1  # where we are not sure
	df.loc[df["GT_aggr"] == 1, "GT_aggr"] = 0  # nowhere is raining
	df.loc[df["GT_aggr"] == 2, "GT_aggr"] = -1  # at one rain gauge it is raining, remaining detect no rain
	df.loc[df["GT_aggr"] == 4, "GT_aggr"] = -1
	df.loc[df["GT_aggr"] == 8, "GT_aggr"] = 1  # all rain gauges detect rain

	# re-adjust rain gauges GT to -1, 0 1
	for rg in rain_gauges:
		df.loc[:, f"{rg}_GT"] -= 1

	df.loc[:, rain_gauges] = df.loc[:, rain_gauges].rolling("5min", min_periods=1).mean()
	# save result
	df.to_pickle(output_path_pkl)
	df.to_csv(output_path_csv)


def main():
	"""Calculate reference. Temperature is not considered, reference will be filtered based on the cml data."""

	path_to_rain_gauges_data = pathlib.Path("D:/Bakalarka/data/rain_gauges/rain_gauges.csv")
	resample_time = "1min"
	time_res = 15
	high_thres = .5
	low_thres = .1

	# output file path
	output_path_pkl = pathlib.Path(f"D:/Bakalarka/data") / f"reference_{resample_time}_resolution_5_min_rolling_mean.pkl"
	output_path_csv = pathlib.Path(f"D:/Bakalarka/data") / f"reference_{resample_time}_resolution_5_min_rolling_mean.csv"

	load_rg_calculate_reference(path_to_rain_gauges_data, resample_time, time_res, low_thres, high_thres, output_path_pkl, output_path_csv)


if __name__ == "__main__":
	main()
