import datetime
import numpy as np
import pandas as pd
import pathlib

def subtract_minimum_from_previous_period(df: pd.DataFrame, period: int):

	df -= df.rolling(period).min().shift(1,axis=0)
	# Delete first period rows, there are only nan values
	df.drop(df.index[range(0,period)], inplace=True)


def prepare_data_for_thresholding(path_to_data, output_path, time_period):
	print(f"reading file from {path_to_data}")
	df = pd.read_pickle(path_to_data)

	# select only cml data
	CML = df.loc[:, "545":"avg_cml"].copy()
	# substract minimum from moving time window interval
	subtract_minimum_from_previous_period(CML, time_period)
	# merge together with the rest
	CML = CML.join(df.loc[:,"D10":], how="inner")
	# drop any rows where are only NaN values
	CML.dropna(axis=0, how="all", inplace=True)
	# save result
	print(f"saving result in {output_path}")
	CML.to_pickle(output_path)


def main():
	path_to_data = pathlib.Path("/Volumes/zaloha/Bakalarka/data/prepared_data_15_res_gt.pkl")
	output_path = pathlib.Path("/Volumes/zaloha/Bakalarka/data/threshold_2d_15min_res_gt_data.pkl")
	time_period = 24 * 60 * 2  # from each datapoint minimum from this time window is subtracted, resolution is one minute -> 24 * 60 = 1day
	
	prepare_data_for_thresholding(path_to_data, output_path, time_period)


if __name__ == "__main__":
	main()