
import datetime
import pandas as pd
import pathlib


def split_data(data: pd.DataFrame, nr_samples: int, list1: list, list2: list):
    list1.append((data.iloc[: nr_samples,:], nr_samples-239))


    rest_samples = data.iloc[nr_samples: , :].shape[0] - 239
    print(f"nr_samples: {nr_samples} rest_samples: {rest_samples}")
    if rest_samples > 0:
        list2.append((data.iloc[nr_samples: , :], rest_samples))


def main():
    dir_path = pathlib.Path("/Volumes/zaloha/Bakalarka/data/CML_data_2_min")

    dirs = [directory for directory in dir_path.iterdir() if directory.relative_to(dir_path).as_posix() in cml_list]
    cml_list = ["56", "444", "545", "374"]

    all_files = []
    for directory in dirs:
        files = [file for file in directory.glob("*.csv")]
        all_files.append(files)

    all_data = []
    nr_all_samples = 0
    for cml_files in all_files:
        data_list = []
        for cml_data in cml_files:
            df = pd.read_csv(cml_data)
            df.time = [datetime.datetime.fromisoformat(t) for t in df.time]
            df = df.set_index("time")
            data_list.append((df, df.shape[0]- 239))
            nr_all_samples += df.shape[0]- 239

        all_data.append(data_list)

    training_list = []
    val_list = []
    test_list = []

    for cml_data in all_data:
        training_counter, val_counter, test_counter = 0, 0, 0

        nr_samples = sum(x[1] for x in cml_data)

        training_samples_count = int(nr_samples * 0.5)
        val_samples_count = int((nr_samples - training_samples_count) / 2)
        test_samples_count = nr_samples - training_samples_count - val_samples_count

        for data, count in cml_data:
            if training_counter < training_samples_count:
                if training_counter + count <= training_samples_count:
                    training_list.append((data, count))
                    training_counter += count
                else:
                    print(f"training counter: {training_counter} training {training_samples_count}, count: {count}")
                    split_data(data, training_samples_count-training_counter, training_list, val_list)
                    training_counter = training_samples_count

            elif val_counter < val_samples_count:
                if val_counter + count <= val_samples_count:
                    val_list.append((data, count))
                    val_counter += count
                else:
                    print(f"val_counter: {val_counter} val: {val_samples_count}, count: {count}")
                    split_data(data, val_samples_count-val_counter, val_list, test_list)
                    val_counter = val_samples_count
            else:
                test_list.append((data, count))
                test_counter += count


    print(sum(x[1] for x in test_list) + sum(x[1] for x in val_list) + sum(x[1] for x in training_list))
    print(nr_all_samples)



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
