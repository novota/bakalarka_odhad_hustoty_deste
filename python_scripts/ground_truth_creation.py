import pandas as pd
import pathlib
import numpy as np
import datetime


def subtract_minimum_from_previous_period(df: pd.DataFrame, period: int = 2*24*60):

	df -= df.rolling(period).min().shift(1,axis=0)
	# Delete first period rows, there are only nan values
	df.drop(df.index[range(0,period)], inplace=True)


def create_ground_truth(path_to_data, time_res, high_tres, low_tres, output_path_pkl, output_path_csv):
	df = pd.read_pickle(path_to_data)
	rain_gauges = ["D10", "D13", "D22"]

	# calculate time_res
	d = df.loc[:, "D10":"average_rain_gauges"].rolling(time_res).max()  # TODO: This .max() has huge impact on gt calculation, maybe min() or ang() would work better
	# dont drop anything
	# d = d.loc[d["D10"].notna() & d["D13"].notna() & d["D22"].notna() & d["average_rain_gauges"].notna()]

	for rg in rain_gauges:

		# apply rules
		d.loc[d[rg] >= high_tres, f"{rg}_GT"] = 2
		d.loc[d[rg] < low_tres, f"{rg}_GT"] = 1
		d.loc[(d[rg] < high_tres) & (d[rg] >= low_tres), f"{rg}_GT"] = 0
		d.loc[d[rg].isna(), f"{rg}_GT"] = 0  # TODO: this might not be necessary because rain gauges nan values get discarded before training

		# append as new column to existing data frame
		df = df.join(d[f"{rg}_GT"], how="outer")

	# it rains only if all rain gauges values are above high_thres
	df["GT_aggr"] = df["D10_GT"] * df["D13_GT"] * df["D22_GT"]
	# if it rains everywhere, result is 6, if at least one rain gauge is not sure, reuslt is 0
	df.loc[df["GT_aggr"] == 0, "GT_aggr"] = -1  # where we are not sure
	df.loc[df["GT_aggr"] == 1, "GT_aggr"] = 0  # nowhere is raining
	df.loc[df["GT_aggr"] == 2, "GT_aggr"] = -1  # at one rain gauge it is raining, remaining detect no rain
	df.loc[df["GT_aggr"] == 4, "GT_aggr"] = -1
	df.loc[df["GT_aggr"] == 8, "GT_aggr"] = 1  # all rain gauges detect rain

	# re-adjust rain gauges GT to -1, 0 1
	for rg in rain_gauges:
		df.loc[:, f"{rg}_GT"] -= 1

	# save result
	df.to_pickle(output_path_pkl)
	df.to_csv(output_path_csv)

def main():
	# input file path
	path_to_data = pathlib.Path("/Volumes/zaloha/Bakalarka/data/prepared_data.pkl")
	# define transformation parameters
	time_res = 5  # in minutes
	high_thres = .5
	low_thres = .1

	# output file path
	output_path_pkl = pathlib.Path(f"/Volumes/zaloha/Bakalarka/data") / f"prepared_data_{time_res}_res_{high_thres}_high_{low_thres}_low_max_gt.pkl"
	output_path_csv = pathlib.Path(f"/Volumes/zaloha/Bakalarka/data") / f"prepared_data_{time_res}_res_{high_thres}_high_{low_thres}_low_max_gt.csv"
	
	create_ground_truth(path_to_data, time_res, high_thres, low_thres, output_path_pkl, output_path_csv)



if __name__ == "__main__":
	main()