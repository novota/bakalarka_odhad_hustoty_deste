import datetime
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pathlib


def eval_treshold(X_data, Y_data, tre):
	if X_data.shape[0] != Y_data.shape[0]:
		print(f"shapes of Xdata and Y data must be the same, but they are not. Xdata shape is: {X_data.shape} , Ydata shape is: {Y_data.shape}")
		return None

	true_positive = int(np.int8((X_data >= tre) & (Y_data is True)).sum())
	false_positive = int(np.int8((X_data >= tre) & (Y_data is False)).sum())
	false_negative = int(np.int8((X_data < tre) & (Y_data is True)).sum())

	# print(f"true positive {type(true_positive)}, false positive {false_positive}, false negative {false_negative}")
	if true_positive == 0:
		return 0

	precision = true_positive / (true_positive + false_positive)
	recall = true_positive / (true_positive + false_negative)
	f1_score = 2 * precision * recall / (precision + recall)

	return f1_score


def find_treshold_hyperparams(train, display_progress=False):
	# lets do some coarse grid search
	best_tre = 0
	best_f1_score = 0
	# tresholds = np.linspace(train.iloc[:,0].min(), train.iloc[:,0].max(), 100)
	tresholds = np.linspace(-10, 10, 50)
	f1_scores = []

	for i, tre in enumerate(tresholds):
		# evaluate on train and validation set
		f1_score = eval_treshold(train.iloc[:,0], train.iloc[:,1], tre)
		f1_scores.append(f1_score)

		if f1_score is not None and f1_score > best_f1_score:
			best_f1_score = f1_score
			best_tre = tre
			if display_progress:
				print(f"best f1 score in corse search for treshold: {tre} is: {f1_score} %")

	# take the best result sofar and search in its vicinity for the best treshold
	step_dist = abs(tresholds[0] - tresholds[1])
	tresholds2 = np.linspace(best_tre - step_dist, best_tre + step_dist, 100)

	for i, tre in enumerate(tresholds2):
		f1_score = eval_treshold(train.iloc[:,0], train.iloc[:,1], tre)

		if f1_score is not None and f1_score > best_f1_score:
			best_f1_score = f1_score
			best_tre = tre
			if display_progress:
				print(f"fine search accuracy for treshold: {tre} is: {f1_score} %")

	# plot results
	accuracy = np.array(f1_scores)
	if display_progress:
		plt.plot(tresholds, accuracy, "r--")
		plt.grid(axis="both")
		plt.show()
	return best_f1_score, best_tre


def find_best_thresholds(path_to_data: pathlib.Path):
	df = pd.read_pickle(path_to_data)
	cml_cols = df.loc[:,"545":"avg_cml"].columns

	results = {}
	for cml in cml_cols:
		print(f"cml {cml}")

		gt = "GT_aggr"
		test_data = df.loc[:, [cml, gt]]
		test_data = test_data.loc[test_data[cml].notna()]

		# divide into training and test data
		rows, cols = test_data.shape
		train = test_data.iloc[: int(rows*0.7),:]
		test = test_data.iloc[int(rows*0.7):,:]

		# find the best treshold
		f1_score, treshold = find_treshold_hyperparams(train)

		# compute accuracy on test data
		f1_test_score = eval_treshold(test.iloc[:,0], test.iloc[:,1], treshold)

		# save results
		print(f"results for cml {cml}")
		print(f"f1 train score is: {f1_score} f1 test score is: {f1_test_score}")
		results[cml] = (treshold, f1_score, f1_test_score)

	return results


def save_results(results, path_to_save_results):
	df_res = pd.DataFrame(columns=["CML", "threshold", "f1_train_score", "f1_test_score"])
	for key in results:
		treshold, f1_train_score, f1_test_score = results[key]
		df_res = df_res.append({"CML": key, "threshold": treshold, "f1_train_score": f1_train_score, "f1_test_score": f1_test_score}, ignore_index=True)

	df_res.to_pickle(path_to_save_results)


def main():
	filename = "threshold_2d_15min_res_gt_data.pkl"
	path_to_data = pathlib.Path(f"/Volumes/zaloha/Bakalarka/data/{filename}")
	path_to_save_results = pathlib.Path(f"/Volumes/zaloha/Bakalarka/data/results/Threshold_results/{filename}")

	results = find_best_thresholds(path_to_data)
	save_results(results, path_to_save_results)



if __name__ == "__main__":
	main()