import datetime
import numpy as np
import pandas as pd
import pathlib

def load_all_cml_data(path_to_data: pathlib.Path) -> pd.DataFrame:
	"""Read all cml files and save them in one data frame."""

	p = path_to_data.glob('**/*')
	files = [x for x in p if x.is_file() and str(x).endswith("pkl") and str(x).find("._") == -1]
	first = True
	for file in files:
		# find dataset name
		file_name = str(file)[len(str(file.parent)) + 1: str(file).rfind(".")]
		# read dataframe
		df1 = pd.read_pickle(path_to_data / f"{file_name}.pkl")
		df1.rename({"attenuation": file_name}, axis=1, inplace=True)

		if not first:
			df = df.join(df1, how="outer")
		else:
			df = df1
			first = False

	return df


def gather_data_and_save(path_to_cml_data, path_to_temperature_and_wet_dry_classification_data, path_to_rain_gauges_data, wd_file_name,
		temperatures_file_name, gauges_file_name, path_to_final_file):
	"""Gather all data and save it in one file"""

	df = load_all_cml_data(path_to_cml_data)

	# load dry wet classification, temperatures, rain gauges
	wet_dry_df = pd.read_csv(path_to_temperature_and_wet_dry_classification_data / wd_file_name, delimiter=";")
	temp_df = pd.read_csv(path_to_temperature_and_wet_dry_classification_data / temperatures_file_name, delimiter=";")
	gauges_df = pd.read_csv(path_to_rain_gauges_data / gauges_file_name, delimiter=";", low_memory=False)

	# convert time to timestamp
	wet_dry_df.time = [datetime.datetime.fromtimestamp(x) for x in wet_dry_df.time]
	temp_df.time = [datetime.datetime.fromtimestamp(x) for x in temp_df.time]
	gauges_df.time = [datetime.datetime.fromtimestamp(x) for x in gauges_df.time]

	# set timestamp as index
	wet_dry_df = wet_dry_df.set_index("time")
	temp_df = temp_df.set_index("time")
	gauges_df = gauges_df.set_index("time")

	# set non-valid values to NaN
	gauges_df.replace("   NA", np.nan, inplace=True)
	gauges_df[["D10", "D13"]] = gauges_df[["D10", "D13"]].apply(pd.to_numeric)

	# compute mean from duplicated indexes
	gauges_df = gauges_df.groupby(gauges_df.index).mean()
	wet_dry_df = wet_dry_df.groupby(wet_dry_df.index).mean()
	temp_df = temp_df.groupby(temp_df.index).mean()

	# add column with cml mean
	df["avg_cml"] = df.loc[:, :].mean(axis=1)

	# add column with rain gauges mean
	gauges_df["average_rain_gauges"] = gauges_df.loc[:,:].mean(axis=1)

	# join everything together
	df = df.join(gauges_df, how="outer")
	df = df.join(wet_dry_df, how="outer")
	df = df.join(temp_df, how="outer")

	# drop rows with Nan values for all cml
	mask = df.loc[:, "545":"71"].notna()
	mask = mask.any(axis=1)
	df = df.loc[mask]

	# drop rows with temperature lower then 5°C
	df = df.loc[df["temperature"] >= 5]

	print(f"are there duplicated indexes? {df.index.duplicated().any()}")
	print(df.describe())
	# save result
	df.to_pickle(path_to_final_file / f"prepared_data.pkl")


def main():
	# insert correct paths to data here
	path_to_cml_data = pathlib.Path("/Volumes/zaloha/Bakalarka/data/CML_data_1_min")
	path_to_temperature_and_wet_dry_classification_data = pathlib.Path("/Volumes/zaloha/Bakalarka/data/Dry_wet_Classification_and_temperature")
	path_to_rain_gauges_data = pathlib.Path("/Volumes/zaloha/Bakalarka/data/rain_gauges")
	path_to_final_file = pathlib.Path("/Volumes/zaloha/Bakalarka/data/")
	wd_file_name = "dry_wet_classification_Letnany.csv"
	temperatures_file_name = "temperature_Prosek.csv"
	gauges_file_name = "rain_gauges.csv"


	gather_data_and_save(path_to_cml_data, path_to_temperature_and_wet_dry_classification_data,
		path_to_rain_gauges_data, wd_file_name, temperatures_file_name, gauges_file_name, path_to_final_file)


if __name__ == "__main__":
	main()