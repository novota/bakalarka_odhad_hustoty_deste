import math
import numpy as np
import pandas as pd
import pathlib


def get_distance(x, y, x1, y1, x2, y2):
	A = x - x1
	B = y - y1
	C = x2 - x1
	D = y2 - y1

	dot = A * C + B * D
	len_sq = C * C + D * D
	param = -1
	if len_sq != 0:
		param = dot / len_sq

	xx, yy = 0, 0

	if param < 0:
		xx = x1
		yy = y1
	elif param > 1:
		xx = x2
		yy = y2
	else:
		xx = x1 + param * C
		yy = y1 + param * D

	dx = x - xx
	dy = y - yy
	y_km = 111.32 * dy
	x_km = dx * 40075 * math.cos(y) / 360
	# 1° lingitude distance = 40075 km * cos( latitude ) / 360
	return math.sqrt(x_km * x_km + y_km * y_km)


def save_cml_metadata(cml_metadata_path: pathlib.Path, target_path: pathlib.Path):
	df = pd.read_csv(cml_metadata_path, delimiter=";")

	rain_gauges = {"D10": (50.13845, 14.48146667), "D13": (50.11402222, 14.48850833), "D22": (50.16479167, 14.54375278)}

	# resulting dataframe
	d = pd.DataFrame(columns=["longitude", "latitude", "frequency", "polarisation_V", "polarisation_H", "length", "x", "y", "reference", "reference_distance_km"])
	# fill resulting dataframe
	for i in df.index:
		cml1, cml2 = df.loc[i, "id"].split("_")
		cml1_data, cml2_data = [], []

		cml1_data.append(df.loc[i, "longA"])
		cml1_data.append(df.loc[i, "latA"])
		cml1_data.append(df.loc[i, "freqA"])

		polarisation = df.loc[i, "polA"]
		cml1_data.append(1 if polarisation == "V" else 0)
		cml1_data.append(1 if polarisation == "H" else 0)

		cml1_data.append(df.loc[i, "length"])
		cml1_data.append(df.loc[i, "xA"])
		cml1_data.append(df.loc[i, "yA"])

		cml2_data.append(df.loc[i, "longB"])
		cml2_data.append(df.loc[i, "latB"])
		cml2_data.append(df.loc[i, "freqB"])

		polarisation = df.loc[i, "polB"]
		cml2_data.append(1 if polarisation == "V" else 0)
		cml2_data.append(1 if polarisation == "H" else 0)

		cml2_data.append(df.loc[i, "length"])
		cml2_data.append(df.loc[i, "xB"])
		cml2_data.append(df.loc[i, "yB"])

		# calculate nearest reference - shortest distance of a point P0 = (x0, y0) to a line defined by two points
		# P1 = (x1, y1) and P2 = (x2, y2)
		min_distance = 100000000
		ref = None
		x1, y1 = df.loc[i, "longA"], df.loc[i, "latA"]
		x2, y2 = df.loc[i, "longB"], df.loc[i, "latB"]
		for reference in rain_gauges:
			y0, x0 = rain_gauges[reference]
			distance = get_distance(x0, y0, x1, y1, x2, y2)
			if distance < min_distance:
				min_distance = distance
				ref = reference

		cml1_data.append(ref)
		cml1_data.append(min_distance)
		cml2_data.append(ref)
		cml2_data.append(min_distance)

		d.loc[cml1] = cml1_data
		if cml2 == "NA":
			continue
		d.loc[cml2] = cml2_data

		d.to_pickle(target_path / "static_data.pkl")
		d.to_csv(target_path / "static_data.csv")


def main():
	cml_metadata_path = pathlib.Path("/Users/petrnovota/Desktop/ETH/Bakalarka/data/CML_data/cml_metadata.csv")
	target_path = pathlib.Path("/Volumes/zaloha/Bakalarka/data/CML_data_1_min")
	save_cml_metadata(cml_metadata_path, target_path)


if __name__ == "__main__":
	main()
