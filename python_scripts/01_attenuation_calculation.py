import datetime

import numpy as np
import pandas as pd
import pathlib
import typing


def compute_attenuations_and_save_into_new_file(files: typing.List[pathlib.Path], path_to_save_data: pathlib.Path, path_to_temperature: pathlib.Path, sample_freq: int,
												folder_containing_cml_data_path: pathlib.Path):
	# load temperature
	temp_df = pd.read_csv(path_to_temperature, delimiter=";")
	# convert time to timestamp
	temp_df.time = [datetime.datetime.fromtimestamp(x) for x in temp_df.time]
	# set timestamp as index
	temp_df = temp_df.set_index("time")
	# merge duplicate indexes
	temp_df = temp_df.groupby(temp_df.index).mean()
	temp_df = temp_df.resample(f"{sample_freq}min").mean()

	for file in files:
		try:
			find = False
			print(f"current file is {file}")
			df = pd.read_csv(file, delimiter=";")

			folder_name = str(file)[len(str(file.parent)) + 1: str(file).rfind(".")]
			new_dir_path = path_to_save_data / folder_name
			if not new_dir_path.exists():
				new_dir_path.mkdir(parents=True, exist_ok=True)

			# mark invalid values as NaN
			df.loc[((df.txpower <= -99.) & (df.txpower > 101.)) | (df.txpower >= 254.) | (df.rxpower <= -99.), ["txpower", "rxpower"]] = np.nan
			# convert to time series
			df.time = [datetime.datetime.fromtimestamp(x) for x in df.time]
			df = df.reset_index().drop("index", axis=1).set_index("time")
			# compute attenuation and drop rxpower and txpower
			df["attenuation"] = df.txpower - df.rxpower
			df.drop(["rxpower", "txpower"], axis=1, inplace=True)

			# resample to sample_freq min
			df = df.resample(f"{sample_freq}min").mean()

			# join with temperature
			df = df.join(temp_df, how="outer")

			# mark as Nan where temperature is less then 5
			df.loc[df["temperature"] < 5,"attenuation"] = np.nan
			# mark as Nan where temperature is Nan
			df.loc[df["temperature"].isna(),"attenuation"] = np.nan

			# divide into continuous data
			no_nan_values = df.loc[df["attenuation"].notna()]
			# split data only if there are more consecutive nans. resampling to 3 minutes excludes single nan values, i.e. only Nan values persistent for three or more minutes will be counted for split
			tmp = df.resample(f"{sample_freq}min").mean()
			nan_indexes = tmp.loc[tmp.attenuation.isna()].index
			counter = 111
			end = False
			while True:
				go_on = True
				first = True
				# loop while gap of nan values is smaller then four. Nan values with this condition are approximated
				while go_on:
					if nan_indexes.shape[0] > 0:
						if first:
							continuous_data = no_nan_values.loc[:nan_indexes[0]]
							first = False
						else:
							continuous_data = pd.concat([continuous_data, no_nan_values.loc[:nan_indexes[0]]])
						# drop used data
						no_nan_values = no_nan_values.loc[no_nan_values.index > nan_indexes[0]]
					else:
						continuous_data = no_nan_values
						end = True

					# drop used nan indexes
					if no_nan_values.shape[0] > 0:
						indexes_droped_count = nan_indexes[nan_indexes <= no_nan_values.index[0]]

						# if there are up to three nan values between valid data, fill this gap with average of last valid data and first valid data from the next set
						if indexes_droped_count.shape[0] < 4:
							fill_data = df.loc[indexes_droped_count, :]
							fill_data.loc[:, "attenuation"] = (continuous_data.iloc[-1,0] + no_nan_values.iloc[0,0]) / 2
							continuous_data = pd.concat([continuous_data, fill_data])
							print(f"cml {folder_name} counter {counter} NOT dropped {indexes_droped_count.shape[0]}")
						else:
							print(f"cml {folder_name} counter {counter} dropped {indexes_droped_count.shape[0]}")
							go_on = False

						nan_indexes = nan_indexes[nan_indexes > no_nan_values.index[0]]

					else:
						end = True
						go_on = False

				#data starts with nan values
				if continuous_data.shape[0] == 0:
					continue

				# drop nan values
				continuous_data = continuous_data.loc[continuous_data.attenuation.notna()]
				# attenuation is always positive
				continuous_data = continuous_data.abs()
				# if continuous data include more the 8 hours of data, save it, otherwise drop it
				if continuous_data.shape[0] > 240:

					# drop temperature
					continuous_data = continuous_data.drop("temperature", axis=1)
					# rename attenuation to cml name
					cml_file_name = file.relative_to(folder_containing_cml_data_path).as_posix()
					cml_name = cml_file_name[:cml_file_name.find(".")]
					continuous_data.rename({"attenuation": cml_name}, axis=1, inplace=True)
					# save data
					continuous_data.to_pickle(new_dir_path / f"{counter}.pkl")
					continuous_data.to_csv(new_dir_path / f"{counter}.csv")
					counter += 1

				# if there are no more non Nan data, end
				if no_nan_values.shape[0] == 0:
					break
				if end:
					break

		except FileNotFoundError:
			print(f"file {file} not found")
		except AttributeError:
			print(f"rxpower or txpower not in the file {file}")


def main():
	# fill in your path to the data, i.e. the folder which contains cml data. !!!The folder has to contain cml data only!!!
	folder_containing_cml_data = pathlib.Path("D:/Bakalarka/data/CML_data")
	# type in path of folder where newly generated data will be stored
	path_to_save_data = pathlib.Path("D:/Bakalarka/data/CML_data_15_min")
	path_to_temperature = pathlib.Path("D:/Bakalarka/data/Dry_wet_Classification_and_temperature/temperature_Prosek.csv")
	sample_freq = 15  # in minutes

	generator = folder_containing_cml_data.glob('**/*')
	files = [x for x in generator if x.is_file() and str(x).endswith("csv") and str(x).find("._") == -1]
	print(files)

	compute_attenuations_and_save_into_new_file(files=files, path_to_save_data=path_to_save_data, path_to_temperature=path_to_temperature,sample_freq=sample_freq,
				folder_containing_cml_data_path=folder_containing_cml_data)


if __name__ == "__main__":
	main()
