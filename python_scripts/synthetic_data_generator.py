import datetime
import numpy as np
import pandas as pd
import pathlib
import matplotlib
import matplotlib.pyplot as plt
import random


def nabehova_funkce(end_diff, delka_hrany):
	res = np.linspace(1, delka_hrany, delka_hrany, dtype=int) * (end_diff / delka_hrany)
	return res

def konecny_utlum(start_diff, delka_hrany):
	res = - np.linspace(1, delka_hrany, delka_hrany, dtype=int) * (start_diff / delka_hrany)
	return res

def waa_utlum(total_decrease, delka_hrany):
	"""Total decrease is the difference between current signal strength and final signal strength."""
	res = -np.linspace(-1, 30, 32, dtype=int)
	res = np.exp(res[0: delka_hrany])  # the sum of this is ca 4.3
	res = res * total_decrease / res.sum()  # multiply with coefficient so thet sum of all elements is equal to end_diff
	# calculate cumulative decrease for each value
	i = len(res)
	while i > 0:
		res[i-1] = res[:i].sum()
		i -= 1

	return res

def get_rain_window(idxs, gt, rain_length):
	middle = random.choice(idxs)
	start, end = middle - rain_length//2, middle + rain_length//2
	try:
		if gt[start: end].any():  # this never triggers index out of bounds even if end is out of bounds
			return get_rain_window(idxs, gt, rain_length)
		gt[end] = gt[end]  # this triggers index out of bounds if end out of bounds
	except IndexError:
		return get_rain_window(idxs, gt, rain_length)

	return start, end




def main():
	total_rain_events = 84
	baseline = 59
	signal_length = 1_027_296
	signal = np.zeros(signal_length)
	min_rain_length, max_rain_length = 10, 1440
	min_rain_intensity, max_rain_intensity = 4, 20
	waa = 2
	start_end_idxses = []

	idxs = np.linspace(0, signal_length - 1, signal_length, dtype=int)
	gt = np.array([False for _ in range(signal_length)])

	signal += baseline

	while total_rain_events > 0:
		rain_length = random.randint(min_rain_length, max_rain_length)
		rain_intensity = random.randint(min_rain_intensity, max_rain_intensity)
		max_start_rain_time = 30 if 30 < rain_length else rain_length // 2
		start_rain_time = random.randint(4, max_start_rain_time)
		stop_rain_time = random.randint(10, 30)

		# find next rain event start and end
		start, end = get_rain_window(idxs, gt, rain_length)
		gt[start:end] = True

		# store start end save indexes
		start_end_idxses.append((start, end))

		# create start of the rain
		signal[start: start+start_rain_time] += nabehova_funkce(rain_intensity, start_rain_time)
		signal[start+start_rain_time: end] += rain_intensity

		idx_start = start + start_rain_time
		spacing = 5
		idx_end = idx_start + spacing

		max_step_up = rain_intensity / 2
		max_step_down = - max_step_up

		rain_end = end - stop_rain_time
		threshold = 500


		while idx_end <= rain_end:
			# choose increase or decrease
			rnd = random.randint(1,1000)
			if rnd > threshold:
				# adjust threshold
				threshold += 50
				# increase
				increase = abs(random.gauss(0, 0.5)) * max_step_up

				# limit the maximum attenuation to 120
				if signal[idx_start] + increase > 120:
					increase *= -1

				#increase = random.random() * max_step_up
				signal[idx_start: idx_end] += nabehova_funkce(increase, spacing)
				signal[idx_end: end] += increase

			else:
				# adjust threshold
				threshold -= 50
				# decrease
				decrease = abs(random.gauss(0, 0.5)) * max_step_down

				# if decrease would go under the baseline plus waa, make it an increase
				decrease *= -1 if signal[idx_start] + decrease < baseline + waa else 1
				#decrease = random.random() * max_step_down
				signal[idx_start: idx_end] += konecny_utlum(-decrease, spacing)
				signal[idx_end: end] += decrease

			idx_start = idx_end
			idx_end = idx_start + spacing

		signal[idx_start: end] += konecny_utlum(signal[idx_start]-baseline, end-idx_start)
		gt[(idx_start+end) // 2: end] = False

		total_rain_events -= 1

	# add baseline attenuation increase between each raining periods

	# add noise
	noise = np.random.normal(0,0.1,signal_length)
	signal += noise

	# add synthetic data to pandas dataframe which hold all data
	data_path = pathlib.Path("/Volumes/zaloha/Bakalarka/data/prepared_data_15_res_gt_running_time_window.pkl")
	data_save_csv = pathlib.Path("/Volumes/zaloha/Bakalarka/data/data_for_nn/prepared_data_15_res_gt.csv")
	data_save_pkl = pathlib.Path("/Volumes/zaloha/Bakalarka/data/prepared_data_15_res_gt.pkl")
	df = pd.read_pickle(data_path)

	df["synthetic_data"] = signal
	df["synthetic_GT"] = gt
	df.to_csv(data_save_csv)
	df.to_pickle(data_save_pkl)
	




if __name__ == "__main__":
	main()