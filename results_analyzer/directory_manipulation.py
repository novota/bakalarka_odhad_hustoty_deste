import pathlib


def clean_dir(directory: pathlib.Path):
	"""Delete all files in a directory."""

	files = [file for file in directory.glob("*.*")]
	for file in files:
		file.unlink()


def create_dir(parent_dir: pathlib.Path, new_dir_name: str):
	"""Create new directory in parent directory. If it already exists, do nothing."""

	new_dir_path = parent_dir / new_dir_name
	if not new_dir_path.exists():
		new_dir_path.mkdir(parents=True, exist_ok=True)

	return new_dir_path


def create_new_dir(local_parent_directory, model_type: str, experiment_description: str):
	"""Create new directory which start with next number that has not been used yet. Directories are named xxx_{directory_description}."""

	# find all directories
	current_dir = local_parent_directory / model_type
	directories = [directory.relative_to(local_parent_directory / model_type) for directory in current_dir.iterdir() if directory.is_dir()]

	if not directories:
		new_dir = current_dir / f"001_{experiment_description}"
		new_dir.mkdir(parents=True, exist_ok=True)
		return new_dir
	# sort in ascending order
	directories = sorted(directories)

	# pick the last entry and get its number
	last_dir = str(directories[len(directories) - 1])
	dir_nr = int(last_dir[:last_dir.find("_")])

	# create new directory name
	dir_nr += 1

	# convert to string and find its length, dir counters have three positions
	dir_nr = str(dir_nr)
	new_dir_name = ""
	for _ in range(3 - len(dir_nr)):
		new_dir_name += "0"

	# append dir number after leading zeros and dir description
	new_dir_name = f"{new_dir_name}{dir_nr}_{experiment_description}"
	new_dir = current_dir / new_dir_name

	# create new dir
	new_dir.mkdir(parents=True, exist_ok=True)
	return new_dir


def create_dirs_for_failed_predictions(working_dir: pathlib.Path, rain_intensities: list):
	# result will be saved in /failed_predicitions_raw_data/ folder, check whether it exist, if not, create it
	fig_save_path = create_dir(working_dir, "failed_predictions_raw_data")

	# create false positive and false negative folders
	false_positive_path = create_dir(fig_save_path, "false_positives")
	false_negative_path = create_dir(fig_save_path, "false_negatives")

	# inside false positive and negative dirs create one dir for each rain intensity
	false_positive_intensities_dirs = {}
	false_negative_intensities_dirs = {}

	for idx, directory in enumerate([false_positive_path, false_negative_path]):
		# create new folder for each rain intensity
		for intensity in rain_intensities:
			if idx == 0:
				false_positive_intensities_dirs[intensity] = create_dir(directory, f"min_intensity_{int(intensity*10)}")
			else:
				false_negative_intensities_dirs[intensity] = create_dir(directory, f"min_intensity_{int(intensity*10)}")

	return false_positive_intensities_dirs, false_negative_intensities_dirs