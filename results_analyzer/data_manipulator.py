
import datetime
import math
import random
import pandas as pd
import pathlib
import paths
import typing


PREDICTIONS = "predictions"
REFERENCE = "reference"
RAIN_INTENSITIES_REG = [0, 1., 5., 15., 25., 35., 300.]
RAIN_INTENSITIES_CLASS = [0, 2, 4, 6, 8, 10, 12]
FALSE_POSITIVE = "false_positive"
FALSE_NEGATIVE = "false_negative"


def get_xticks():
	"""Get x axis ticks from rain intensities."""

	result = []
	for idx in range(len(RAIN_INTENSITIES_REG) - 1):
		result.append(f"{RAIN_INTENSITIES_REG[idx]} <= y < {RAIN_INTENSITIES_REG[idx + 1]}")
	return result

def get_rain_gauge_from_gt(gt_name: str):
	"""Get name of rain gauge from ground truth.

	D10_GT -> D10
	D13_GT -> D13
	D22_GT -> D22
	GT_aggr -> average_rain_gauges
	"""
	if gt_name == "GT_aggr":
		return "average_rain_gauges"
	return gt_name[:gt_name.find("_")]


def get_mse(df: pd.DataFrame):
	"""Calculate average MSE."""

	return math.sqrt(((df.loc[:, PREDICTIONS]- df.loc[:, REFERENCE])**2).sum() / df.shape[0])


def get_nmse(df: pd.DataFrame):
	"""Calculate average MSE."""

	return math.sqrt(((df.loc[:, PREDICTIONS] - df.loc[:, REFERENCE])**2).sum() / df.shape[0]) / df.loc[:, REFERENCE].mean()


def get_metric_distribution(df: pd.DataFrame, cml_name: int, predictions: str, reference: str):
	"""Calculate F1 scores for increasing rain intensities.

	:return: list[tuple(intensity, f1 score)]
	"""
	result = []
	for intensity in RAIN_INTENSITIES_CLASS:
		mask = (df["average_rain_gauges"] >= intensity) & (df["cml"] == cml_name)
		tmp = df.loc[mask, [predictions, reference]]
		if reference == REFERENCE:
			result.append((intensity, get_f1_score(tmp)))  # tuple(minimal intensity, f1_score)
		elif reference == REFERENCE:
			result.append((intensity, get_mse(tmp)))

	return result


def get_metric_distribution_regions(df: pd.DataFrame, cml_name: int, predictions: str, reference: str, nmse: bool):
	"""Calculate f1 score or rmse for increasing rain intensities.

	:return: list[tuple(intensity, f1 score)]
	"""
	result = []
	for idx in range(len(RAIN_INTENSITIES_REG) - 1):
		if isinstance(cml_name, int):
			mask = (df[REFERENCE] >= RAIN_INTENSITIES_REG[idx]) & (df[REFERENCE] < RAIN_INTENSITIES_REG[idx + 1]) & (df["cml"] == cml_name)
		# all cmls together
		else:
			mask = (df[REFERENCE] >= RAIN_INTENSITIES_REG[idx]) & (df[REFERENCE] < RAIN_INTENSITIES_REG[idx + 1])
		tmp = df.loc[mask, [predictions, reference]]

		if nmse:
			result.append((RAIN_INTENSITIES_REG[idx], RAIN_INTENSITIES_REG[idx + 1], get_nmse(tmp)))
		else:
			result.append((RAIN_INTENSITIES_REG[idx], RAIN_INTENSITIES_REG[idx + 1], get_mse(tmp)))

	return result


def add_cml_attenuation(df: pd.DataFrame, cml_df: pd.DataFrame, cml_name: str) -> pd.DataFrame:
	"""Create dataframe that consists of test data part and columns are:
	predictions, reference, cml_data, Average_rain_gauges.

	:param cml_df: dataframe containing cml attenuation data for all cmls
	:param df: dataframe to which data will be added, as input it contains [predictions, reference, index]
	:param cml_name: the name of the cml
	:return: new dataframe with reference information
	"""
	idx = df.index.values
	cml_data = cml_df.loc[idx[0]: idx[-1], [cml_name, "average_rain_gauges"]]
	df = df.join(cml_data, how="left")
	return df


def get_f1_score(df: pd.DataFrame):
	"""Calculate F1 score."""

	true_positive = ((df[PREDICTIONS] == 1) & (df[REFERENCE] == 1)).sum()
	false_positive = ((df[PREDICTIONS] == 1) & (df[REFERENCE] == 0)).sum()
	false_negative = ((df[PREDICTIONS] == 0) & (df[REFERENCE] == 1)).sum()

	if true_positive == 0:
		return 0

	precision = true_positive / (true_positive + false_positive)
	recall = true_positive / (true_positive + false_negative)
	f1_score = 2 * precision * recall / (precision + recall)
	return f1_score


def get_f1_distribution(df: pd.DataFrame, minimal: bool):
	"""Calculate F1 scores for increasing rain intensities.

	:return: list[tuple(intensity, f1 score)]
	"""
	result = []
	for intensity in RAIN_INTENSITIES_CLASS:
		if minimal:
			mask = (df["average_rain_gauges"] >= intensity)
		else:
			mask = (df["average_rain_gauges"] <= intensity)
		tmp = df.loc[mask, [PREDICTIONS, REFERENCE]]
		result.append((intensity, get_f1_score(tmp)))  # tuple(minimal intensity, f1_score)

	return result


def find_indexes_with_condition(df: pd.DataFrame, false_positive: bool=True):
	"""Find up to five indexes that are not close to each other and that fulfil max rain intensity condition and either false positive or false negative prediction condition."""

	result = {}

	for intensity in RAIN_INTENSITIES_CLASS:
		mask = (df["predictions"] == int(false_positive)) & (df[REFERENCE] == int(not false_positive)) & (df["average_rain_gauges"] >= intensity)
		tmp = df.loc[mask]
		if tmp.shape[0] == 0:
			break
		rows = tmp.shape[0]
		result[intensity] = []

		# pick 5 different indexes, split rows in 5 sections and find random index in each
		for i in range(1, 6):
			lower_bound = int(rows * (i - 1) / 5)
			upper_bound = int(rows * i / 5) - 1
			upper_bound = 0 if upper_bound < 0 else upper_bound
			upper_bound = lower_bound if lower_bound > upper_bound else upper_bound

			idx = random.randint(lower_bound, upper_bound)
			# check for index validity
			if idx >= rows:
				print(f"index: {idx} is not in rows: {rows} range")
				continue

			# get date time index
			idx = tmp.index[idx]
			offset = 0.15
			start, end = idx - datetime.timedelta(days=offset), idx + datetime.timedelta(days=offset)

			while ~df.index.isin([start, end]).any():
				offset += 0.025
				start, end = idx - datetime.timedelta(days=offset), idx + datetime.timedelta(days=offset)
				if offset == 1:
					offset = 0.2
					start, end = idx - datetime.timedelta(days=offset), idx + datetime.timedelta(days=offset)
					break

			result[intensity].append((start, end))

	return result


def adjust_data_for_plot(data: typing.List[pd.DataFrame]):
	"""Adjust data in the list so that they can be plotted easily, i.e. their mean values should be 3 units apart."""

	averages = [x.mean() for x in data]
	baseline = [x*3 for x in range(len(data))]
	diff = [a-b for a,b in zip(averages, baseline)]
	data = [df - d for df, d in zip(data, diff)]
	return data


def get_reference_for_cml(self, cml):
	"""Find reference for each cml in cmls based on the static data."""

	rg = self.static_data.loc[self.static_data.index == 55, "reference"].values[0]
	ref = f"{rg}_GT"
	result = self.static_data.loc[self.static_data.index == 55, "reference"].values[0], ref
	return result


def multiply_with_classification(df: pd.DataFrame):
	"""Multiply regression results with classification."""

	df_class = pd.read_csv(paths.CLASSIFICATION_RESULT)
	df.loc[df_class.index, PREDICTIONS] = df.loc[df_class.index, PREDICTIONS] * df_class[PREDICTIONS]
	return df
