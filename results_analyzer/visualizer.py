from directory_manipulation import create_dir, create_dirs_for_failed_predictions
from data_manipulator import get_f1_distribution, find_indexes_with_condition, adjust_data_for_plot, RAIN_INTENSITIES_REG, RAIN_INTENSITIES_CLASS, PREDICTIONS, REFERENCE, \
	get_metric_distribution, get_metric_distribution_regions, get_xticks

import datetime
import pathlib
import paths

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd

FONTSIZE = 30
LINEWIDTH = 4


def create_training_progress_visualization(index: pd.Index, training_data, val_data, cml_name: str, working_dir: pathlib.Path):
	"""Create and save training progress visualization."""

	# result will be saved in /training_progress/ folder, check whether it exist, if not, create it
	fig_save_path = create_dir(working_dir, "training_progress")

	# normalize training and validation losses
	training_data = training_data / training_data.max()
	val_data = val_data / val_data.max()

	# create file name
	fig_save_path = fig_save_path / f"{cml_name}_tpv"

	# plot and save result
	plt.figure(figsize=(40, 30))
	# dividing training loss by three, because training data are 3 times bigger
	plt.plot(index, training_data, "b", val_data, "r", linewidth=LINEWIDTH)

	# plot style
	plt.xticks(fontsize=FONTSIZE)
	plt.yticks(fontsize=FONTSIZE)
	plt.grid(axis="both")
	# graph description
	plt.xlabel("Epoch", fontsize=FONTSIZE)
	plt.ylabel("Cost", fontsize=FONTSIZE)
	plt.title(f"Training progress", fontsize=FONTSIZE)
	plt.legend([f"training loss", "validation loss"], fontsize=FONTSIZE, loc="upper right")
	plt.savefig(fig_save_path)
	print(f"training progress fig of {cml_name} saved in {fig_save_path}")


def create_nrmse_visualisation(df: pd.DataFrame, cml_name: int, working_dir: pathlib.Path):
	"""Create and save mean square error distribution visualization."""

	# result will be saved in /training_progress/ folder, check whether it exist, if not, create it
	fig_save_path = create_dir(working_dir, "nrmse_distribution")

	# create file name
	fig_save_path = fig_save_path / f"{cml_name}_nrmsed"

	# get F1 score distribution, f1_distribution is list[tuple(minimal intensity, f1 score|
	mse_distribution = get_metric_distribution_regions(df, cml_name, PREDICTIONS, REFERENCE, nmse=True)

	# plot and save result
	plt.figure(figsize=(40, 30))

	idx = 0
	for intensity_low, intensity_high, mse in mse_distribution:
		plt.bar(idx, mse if mse < 6 else 6, width=0.4)
		# display f1 score above the bar (x-position, y-position, string)
		plt.text(idx - 0.1, mse if mse < 6 else 6, str(round(mse, 2)), fontsize=FONTSIZE, style="oblique")
		idx += 1

	# plot style
	x_ticks = get_xticks()
	plt.xticks(ticks=[x for x in range(len(RAIN_INTENSITIES_REG) - 1)], labels=x_ticks, fontsize=FONTSIZE)

	plt.yticks(fontsize=FONTSIZE)
	plt.grid(axis="both")
	# graph description
	plt.xlabel("Rain rate range [mm/hr]", fontsize=FONTSIZE)
	plt.ylabel("NRMSE", fontsize=FONTSIZE)
	plt.title(f"CML {cml_name}, NRMSE distribution", fontsize=FONTSIZE)
	#plt.legend([f"training loss", "validation loss"], fontsize=FONTSIZE, loc="upper right")
	plt.savefig(fig_save_path)
	print(f"NRMSE distribution fig of {cml_name} saved in {fig_save_path}")


def create_rmse_visualisation(df: pd.DataFrame, cml_name: int, working_dir: pathlib.Path):
	"""Create and save mean square error distribution visualization."""

	# result will be saved in /training_progress/ folder, check whether it exist, if not, create it
	fig_save_path = create_dir(working_dir, "rmse_distribution")

	# create file name
	fig_save_path = fig_save_path / f"{cml_name}_rmsed"

	# get F1 score distribution, f1_distribution is list[tuple(minimal intensity, f1 score|
	mse_distribution = get_metric_distribution_regions(df, cml_name, PREDICTIONS, REFERENCE, nmse=False)

	# plot and save result
	plt.figure(figsize=(40,30))

	idx = 0
	for intensity_low, intensity_high, mse in mse_distribution:
		plt.bar(idx, mse, width=0.4)
		# display f1 score above the bar (x-position, y-position, string)
		plt.text(idx - 0.1, mse - 0.02, str(round(mse, 2)), fontsize=FONTSIZE, style="oblique")
		idx += 1

	# plot style
	x_ticks = get_xticks()
	plt.xticks(ticks=[x for x in range(len(RAIN_INTENSITIES_REG) - 1)], labels=x_ticks, fontsize=FONTSIZE)

	plt.yticks(fontsize=FONTSIZE)
	plt.grid(axis="both")
	# graph description
	plt.xlabel("Rain rate range [mm/hr]", fontsize=FONTSIZE)
	plt.ylabel("RMSE", fontsize=FONTSIZE)
	plt.title(f"CML {cml_name}, RMSE distribution", fontsize=FONTSIZE)
	#plt.legend([f"training loss", "validation loss"], fontsize=FONTSIZE, loc="upper right")
	plt.savefig(fig_save_path)
	print(f"RMSE distribution fig of {cml_name} saved in {fig_save_path}")


def create_f1_score_minimal_rain_intensity_visualization(df: pd.DataFrame, cml_name: str, working_dir: pathlib.Path):
	"""Create and save Success rate distribution visualization."""

	# result will be saved in /training_progress/ folder, check whether it exist, if not, create it
	fig_save_path = create_dir(working_dir, "f1_score_distribution")

	# create file name
	fig_save_path = fig_save_path / f"{cml_name}_min_srd"

	# get F1 score distribution, f1_distribution is list[tuple(minimal intensity, f1 score|
	f1_distribution = get_f1_distribution(df, minimal=True)

	# plot and save result
	plt.figure(figsize=(40, 30))

	for intensity, f1_score in f1_distribution:
		plt.bar(intensity, f1_score, width=0.8)
		# display f1 score above the bar (x-position, y-position, string)
		plt.text(intensity - 0.1, f1_score - 0.02, str(round(f1_score, 2)), fontsize=FONTSIZE, style="oblique")

	# plot style
	plt.xticks(fontsize=FONTSIZE)
	plt.yticks(fontsize=FONTSIZE)
	plt.grid(axis="both")
	# graph description
	plt.xlabel("Minimal rain intensity", fontsize=FONTSIZE)
	plt.ylabel("F1 score", fontsize=FONTSIZE)
	plt.title(f"CML {cml_name}, F1 score distribution", fontsize=FONTSIZE)

	plt.savefig(fig_save_path)
	print(f"F1-score distribution fig of cml {cml_name} saved to {fig_save_path}")


def create_f1_score_maximal_rain_intensity_visualization(df: pd.DataFrame, cml_name: str, working_dir: pathlib.Path):
	"""Create and save Success rate distribution visualization."""

	# result will be saved in /training_progress/ folder, check whether it exist, if not, create it
	fig_save_path = create_dir(working_dir, "f1_score_distribution")

	# create file name
	fig_save_path = fig_save_path / f"{cml_name}_max_srd"

	# get F1 score distribution, f1_distribution is list[tuple(minimal intensity, f1 score|
	f1_distribution = get_f1_distribution(df, minimal=False)

	# plot and save result
	plt.figure(figsize=(40, 30))

	for intensity, f1_score in f1_distribution:
		plt.bar(intensity, f1_score, width=1.5)
		# display f1 score above the bar (x-position, y-position, string)
		plt.text(intensity - 0.1, f1_score - 0.02, str(round(f1_score, 2)), fontsize=FONTSIZE, style="oblique")

	# plot style
	plt.xticks(fontsize=FONTSIZE)
	plt.yticks(fontsize=FONTSIZE)
	plt.grid(axis="both")
	# graph description
	plt.xlabel("Maximal rain intensity", fontsize=FONTSIZE)
	plt.ylabel("F1 score", fontsize=FONTSIZE)
	plt.title(f"CML {cml_name}, F1 score distribution", fontsize=FONTSIZE)

	plt.savefig(fig_save_path)
	print(f"F1-score distribution fig of cml {cml_name} saved to {fig_save_path}")


def plot_failed_predictions(x_axis, y_axis: list, fig_save_path: pathlib.Path, title: str, legend: list):
	"""Plot failed prediction and save it."""

	plt.figure(figsize=(40,30))
	for y in y_axis:
		plt.plot(x_axis, y, linewidth=LINEWIDTH)

	plt.xticks(fontsize=FONTSIZE)
	plt.yticks(fontsize=FONTSIZE)
	plt.grid(axis="both")
	# graph description
	plt.title(title, fontsize=FONTSIZE)
	plt.legend(legend, fontsize=FONTSIZE, loc="upper right")
	# save result
	plt.savefig(fig_save_path)
	plt.close("all")


def create_raw_data_failed_predictions_visualization(df: pd.DataFrame, cml_name: str, working_dir: pathlib.Path):
	"""Graph raw data of failed predictions. Graoh will contain cml data, gt data, predictions and average rain gauges."""

	# inside false positive and negative dirs create one dir for each rain intensity
	false_positive_intensities_dirs, false_negative_intensities_dirs = create_dirs_for_failed_predictions(working_dir=working_dir, rain_intensities=RAIN_INTENSITIES_CLASS)

	false_positive_cases = find_indexes_with_condition(df=df, false_positive=True)
	false_negative_cases = find_indexes_with_condition(df=df, false_positive=False)

	print(f"cml {cml_name} failed predicitons")
	# go through false positive and false negative cases
	for idx, case in enumerate([false_positive_cases, false_negative_cases]):

		# go through minimal rain intensities, each intensity consists of a list with up to 6 tuples: (start_idx, stop_idx)
		for intensity in case:
			# index used in plot name
			name_idx = 1
			print(f"cml {cml_name} failed predictions for intensity {intensity}")

			# go through start_idx, stop_idx, there are up to 6 of them for each rain intensity
			for start_idx, stop_idx in case[intensity]:

				# define path where figure will be saved
				if idx == 0:
					fig_save_path = false_positive_intensities_dirs[intensity] / f"{cml_name}_intensity_{int(intensity*10)}_{name_idx}"
					title = f"CML {cml_name} false positive prediction"
				else:
					fig_save_path = false_negative_intensities_dirs[intensity] / f"{cml_name}_intensity_{int(intensity*10)}_{name_idx}"
					title = f"CML {cml_name} false negative prediction"

				try:
					x_axis = df.loc[start_idx:stop_idx].index
				except KeyError:
					print("something went wrong")
					continue
				try:
					y_axis = [df.loc[start_idx: stop_idx, REFERENCE], df.loc[start_idx: stop_idx, PREDICTIONS],
												df.loc[start_idx: stop_idx, "average_rain_gauges"], df.loc[start_idx: stop_idx, cml_name]]
				except KeyError:
					print("wrong index")
					continue
				legend = ["reference", "prediction", "rain gauges average", "cml_data"]
				y_axis = adjust_data_for_plot(y_axis)
				# plot and save result
				plot_failed_predictions(x_axis, y_axis, fig_save_path, title, legend)

				name_idx += 1
