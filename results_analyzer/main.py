
from scp import SCPClient
import datetime
import paramiko
import pandas as pd
import pathlib
import paths
import subprocess
import time

from ast import literal_eval
from directory_manipulation import create_new_dir, clean_dir
from visualizer import create_training_progress_visualization, create_f1_score_minimal_rain_intensity_visualization, create_raw_data_failed_predictions_visualization, \
	create_nrmse_visualisation, create_rmse_visualisation, create_f1_score_maximal_rain_intensity_visualization
from data_manipulator import add_cml_attenuation, multiply_with_classification

# This MUST be tha same as in conv_net
remote_parent_directory = pathlib.Path("/datagrid/personal/novotp23/bakalarka_data/1/")
remote_status_directory = pathlib.Path("/datagrid/personal/novotp23/bakalarka_data/1/status")
local_parent_directory = pathlib.Path("D:/Bakalarka/data/auto_results_v2/")

RESULTS = "results"
STATUS = "00_status"
MODEL = "model"
THRESHOLDING = "01_thresholding"
SHALLOW_NN = "02_shallow_nn"
CONV_NN = "03_conv_nn"
RECURRENT_NN = "04_recurrent_nn"

CLASSIFICATION = "classification"
REGRESSION = "regression"

mse_metric = True


def create_ssh_client(server, user, password):
	client = paramiko.SSHClient()
	client.load_system_host_keys()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(server, username=user, password=password)  # , disabled_algorithms={'pubkeys': ['rsa-sha2-256', 'rsa-sha2-512']}
	return client


def download_remote_repo(remote_dir, local_dir):
	"""Download remote directory."""
	server = "zorn.felk.cvut.cz"
	user = "novotp23"
	password = "jyktaj-masgiT-2hinmy"

	ssh = create_ssh_client(server, user, password)
	scp = SCPClient(ssh.get_transport())

	scp.get(remote_dir.as_posix(), local_dir.as_posix(), recursive=True)
	ssh.close()


def download_remote_status(working_dir: pathlib.Path):
	global remote_status_directory, local_parent_directory

	download_remote_repo(remote_status_directory, local_parent_directory / STATUS)
	download_remote_repo(remote_status_directory, working_dir / RESULTS)


def end_of_training():
	files = [file for file in (local_parent_directory / STATUS / "status").glob("END.txt")]
	if files:
		return True
	else:
		return False


def wait_for_training_end(wait_time: float, working_dir: pathlib.Path):
	"""Repeatedly download status folder from remote and check existence of END.txt which signals the end of training."""

	while not end_of_training():

		# wait and try again
		print(f"waiting for the end of the training")
		time.sleep(wait_time)
		# Download results folder from remote server
		download_remote_status(working_dir)

	print("training is done.")


def extract_cml_from_file_name(files: list, post_fix: str):
	"""Create a tuple for each element in files tuple: (filepath, cml_name)."""

	for idx, element in enumerate(files):
		file_name = str(element.relative_to(element.parent))
		cml = file_name[: file_name.find(post_fix)]
		# replace element with tuple: (file_path, cml_name)
		files[idx] = (element, cml)


def get_model_type_and_reference(working_dir: pathlib.Path):
	"""Get model type [regression or classification] from END.txt file in results."""

	with open(working_dir / RESULTS / "END.txt", "r") as f:
		model_type = f.readline().rstrip("\n")
		reference = f.readline()
		if model_type != REGRESSION and model_type != CLASSIFICATION:
			print(f"END.txt doesnt include any of [regression, classification], model type unknown!!\nExiting program")
			exit(1)
		if len(reference) == 0:
			print(f"there is no data reference in END.txt!!\nExiting program")
			exit(1)
		return model_type, reference


def run_classification_visualisation(working_dir: pathlib.Path, df: pd.DataFrame, cml_name: str):
	"""Run result analysis for a classification model."""

	create_f1_score_minimal_rain_intensity_visualization(df=df, cml_name=cml_name, working_dir=working_dir)
	create_f1_score_maximal_rain_intensity_visualization(df=df, cml_name=cml_name, working_dir=working_dir)
	create_raw_data_failed_predictions_visualization(df=df, cml_name=cml_name, working_dir=working_dir)


def run_regression_visualisation(working_dir: pathlib.Path, df: pd.DataFrame, cml_name: str):
	"""Run result analysis for a regression model"""

	create_nrmse_visualisation(df, cml_name, working_dir)
	create_rmse_visualisation(df, cml_name, working_dir)


def visualisation_part(working_dir: pathlib.Path):

	# ::::::::::::::::::::::::::::::::: visualization PART :::::::::::::::::::::::::::::::::::::::::: #

	# Training progress visualization
	# Find all files ending with *_training_pregress.csv
	list_of_training_progress = [x for x in (working_dir / "results").glob("*training_progress.csv")]
	# each list element will be a tuple (path, cml_name)
	extract_cml_from_file_name(list_of_training_progress, post_fix="_training_progress.csv")

	# Create visualization of training progress, i.e. training loss and validation loss graph
	for training_progress in list_of_training_progress:
		data_path, cml_name = training_progress
		df = pd.read_csv(data_path)

		class_trn = df.loc[:, "training_loss"].values
		class_val = df.loc[:, "validation_loss"].values

		create_training_progress_visualization(df.index, class_trn, class_val, cml_name, working_dir)

	# :::::::::::::::::::::::::::::::::::GET PREDICTIONS::::::::::::::::::::::::::::::::::::::::::: #
	# get predictions
	list_of_predictions = [x for x in (working_dir / "results").glob("*.csv") if not x.stem.startswith("._") and x.stem.find("training") == -1]
	extract_cml_from_file_name(list_of_predictions, post_fix=".csv")

	# ::::::::::::::::::::::::::::::::: Find out whether it is a classification or regression model::::::::::::::::::::::
	model_type, reference_data_name = get_model_type_and_reference(working_dir=working_dir)

	# read attenuation data for all cmls
	df_cmls = pd.read_pickle(paths.CML_DATA)
	df_cmls = df_cmls.resample("1T").pad()

	# go through predictions
	for prediction in list_of_predictions:
		data_path, cml_name = prediction

		df = pd.read_csv(data_path)
		if df.empty:
			print(f"No predictions data for cml {cml_name}")
			continue
		df.loc[:, "index"] = [datetime.datetime.utcfromtimestamp(x) for x in df["index"].astype("int64").values.tolist()]
		df.drop("Unnamed: 0", axis=1, inplace=True)

		if cml_name == "all":
			pass
			# if model_type == REGRESSION:
			# 	# multiplicate with classification results
			# 	df = multiply_with_classification(df)
		else:
			# sort by index column
			df = df.sort_values("index")
			df.set_index("index", inplace=True)
			# add reference
			df = add_cml_attenuation(df, df_cmls, cml_name)

		if model_type == CLASSIFICATION:
			run_classification_visualisation(working_dir=working_dir, df=df, cml_name=cml_name)

		else:
			run_regression_visualisation(working_dir=working_dir, df=df, cml_name=cml_name)


def main():
	global local_parent_directory, remote_parent_directory

	# if download remote is false, go through all existing folders between given folder nubers and redo all analysis for given model_type folder
	download_remote = False
	folder_start = 25
	folder_end = 40

	# define model type
	model_type = CONV_NN

	if download_remote:
		# define experiment name
		experiment_description = "conv_reg_multiple_cmls_mse_loss_5_min_sum_reference"

		# create new directory where results and analysis will be stored
		working_dir = create_new_dir(local_parent_directory, model_type=model_type, experiment_description=experiment_description)

		# clean local status directory
		clean_dir(local_parent_directory / STATUS / "status")

		# Check for END.txt existence
		wait_for_training_end(wait_time=0.1, working_dir=working_dir)

		# Training is done, download results
		download_remote_repo(remote_parent_directory / RESULTS, working_dir)
		print(f"results repo downloaded to {working_dir}")
		# Download model used in training
		download_remote_repo(remote_parent_directory / MODEL, working_dir)
		print(f"model downloaded to {working_dir}")
		# run analysis
		visualisation_part(working_dir=working_dir)
	else:
		folders = [x for x in (local_parent_directory / model_type).iterdir()]
		working_dirs = []
		# create list of all working dirs
		for folder in folders:
			folder_name = str(folder.relative_to(local_parent_directory / model_type))
			number = int(folder_name[:folder_name.find("_")])
			if folder_start <= number <= folder_end:
				working_dirs.append(folder)

		for working_dir in working_dirs:
			print(f"\n redoing analysis for {working_dir}")
			visualisation_part(working_dir=working_dir)


if __name__ == "__main__":
	main()
