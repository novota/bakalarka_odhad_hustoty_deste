
import pathlib

DATA_SOURCE = pathlib.Path("D:/Bakalarka/data")
DATA_REFERENCE = pathlib.Path("D:/Bakalarka/data/reference_1min_resolution_15_res_0.5_high_0.1_low_max.pkl")
STATIC_DATA = pathlib.Path("D:/Bakalarka/data/CML_data_2_min/static_data.pkl")
REFERENCE_DATA_DIR = pathlib.Path("D:/Bakalarka/data")
CML_DATA = pathlib.Path("D:/Bakalarka/data/prepared_data_15_res_gt.pkl")

CLASSIFICATION_RESULT = pathlib.Path("D:/Bakalarka/data/auto_results_v2/03_conv_nn/001_003_skip_connections_1_day_seq_len_gt_15min_max/results/56.csv")
