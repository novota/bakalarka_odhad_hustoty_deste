
import data_paths
import torch
import torch.nn as nn
import train_class

from all_in_one_dataframe_data import AllInOneDataframe
from continuous_data import ContinuousData
from helper_functions import clear_directory
from models import conv_class, conv_reg_model, conv_reg_habi_type
from shutil import copyfile


def clear_directories() -> None:
	"""Clear all files in results folder and status folder."""

	clear_directory(data_paths.STATUS_DIR)
	clear_directory(data_paths.MODEL_TARGET_DIR)
	clear_directory(data_paths.RESULTS_DIR)

	print(f"directories cleared: \n {data_paths.STATUS_DIR} \n {data_paths.MODEL_TARGET_DIR}")
	

def save_models():
	# save model used for training
	source = data_paths.MODEL_SOURCE_DIR
	destination = data_paths.MODEL_TARGET_DIR

	copyfile(source / "all_in_one_dataframe_data.py", destination / "all_in_one_dataframe_data.py")
	copyfile(source / "continuous_data.py", destination / "continuous_data.py")
	copyfile(source / "models/conv_reg_model.py", destination / "conv_reg_model.py")
	copyfile(source / "data.py", destination / "data.py")
	copyfile(source / "data_paths.py", destination / "data_paths.py")
	copyfile(source / "helper_functions.py", destination / "helper_functions.py")
	copyfile(source / "interface.py", destination / "interface.py")
	copyfile(source / "main.py", destination / "main.py")
	copyfile(source / "mse_loss.py", destination / "mse_loss.py")
	copyfile(source / "train_class.py", destination / "train_class.py")

	print(f"all training scripts copied from {source} to {destination}")


def main():
	# clear directories
	clear_directories()

	lr = [0.00000003, 0.0000001]
	epochs = 2
	batch_size = 200
	# cmls = ["547", "444", "56", "415", "105"]
	cmls = ["56", "444", "547"]
	# device
	device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
	
	# define model
	get_model = conv_class.get_model
	# get_model = conv_reg_habi_type.get_model

	# data
	data = AllInOneDataframe(cmls=cmls, training_data_size=0.7, validation_data_size=1)
	# data = ContinuousData(cmls=cmls, training_data_size=0.7, validation_data_size=1, seq_len=1440)

	# training
	training = train_class.Training(get_model_func=get_model, data=data, batch_size=batch_size, epochs=epochs)
	try:
		training.run_one_cml_at_time_training(lr=lr[0], device=device)
		# training.run_all_cmls_at_once_training(lr=lr[0], device=device)
	except KeyboardInterrupt:
		print("training interrupted by user, saving models and quitting")
	
	save_models()


if __name__ == "__main__":
	main()
