import pathlib

"""This file contains paths to all data necessary for the program to work."""

REFERENCE_DATA = pathlib.Path("/datagrid/personal/novotp23/bakalarka_data/reference/reference_1min_resolution_15_res_0.5_high_0.1_low_max.csv")
ALL_DATA = pathlib.Path("/datagrid/personal/novotp23/bakalarka_data/attenuation/prepared_data_15_res_gt.csv")
STATIC_DATA = pathlib.Path("/datagrid/personal/novotp23/bakalarka_data/static_data.csv")
DYNAMIC_DATA_FOLDER = pathlib.Path("/datagrid/personal/novotp23/bakalarka_data/attenuation/CML_data_1_min")

STATUS_DIR = pathlib.Path("/datagrid/personal/novotp23/bakalarka_data/status/")
RESULTS_DIR = pathlib.Path("/datagrid/personal/novotp23/bakalarka_data/results/")
MODEL_TARGET_DIR = pathlib.Path("/datagrid/personal/novotp23/bakalarka_data/model/")
MODEL_SOURCE_DIR = pathlib.Path("/tmp/pycharm_project_930/")

