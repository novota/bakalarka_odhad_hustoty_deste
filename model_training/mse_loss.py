import torch
import torch.nn as nn

class MSEScaledLoss(nn.Module):
	def __init__(self, delta_s: float, delta_r: float):
		super(MSEScaledLoss, self).__init__()
		# how much we should pay loss for no object (noobj) and the box coordinates (coord)
		self.delta_s = delta_s
		self.delta_r = delta_r

	def forward(self, predictions: torch.Tensor, target: torch.Tensor):
		"""Compute mean square error (MSE) weighted by rain distribution factor (RDF) RDF = 1 - delta_s * exp(-delta_r * target)"""
		# filter out nan reference values

		predictions = predictions.squeeze(dim=1)[~target.isnan()]
		target = target[~target.isnan()]

		# compute loss
		Jmse = torch.square(predictions - target)
		# rain distribution factor
		RDF = 1 - self.delta_s * torch.exp(-self.delta_r * target)

		return torch.sum(Jmse * RDF) / predictions.shape[0]
