import data_paths
import interface
import pandas as pd
import typing


class AllInOneDataframe(interface.DataBase):
	all_data: pd.DataFrame
	rg_data_and_reference = ["D10", "D13", "D22", "average_rain_gauges", "D10_GT", "D13_GT", "D22_GT", "GT_aggr"]

	def __init__(self, cmls: typing.List[str], training_data_size: float, validation_data_size: float, reference: typing.List[typing.Tuple[str, str]] = None):
		"""Initialize class instance.

		:param cmls: Cml names that will be used for training
		:param training_data_size: the size of training data as a number in range [0, 1]
		:param validation_data_size: the size of validation data set as a number in range [training_data_size, 1], the rest will be used as test data
		"""

		super(AllInOneDataframe, self).__init__(cmls=cmls, training_data_size=training_data_size, validation_data_size=validation_data_size, reference=reference)

		self.load_all_data()
		self.drop_not_relevant_cmls()
		self.drop_not_relevant_references()

	def get_training_data(self) -> typing.List[typing.List[pd.DataFrame]]:
		"""Get training data in form of list of lists where each list contains data for one cml."""

		return self.split_into_list_of_lists(training=True)

	def get_validation_data(self) -> typing.List[typing.List[pd.DataFrame]]:
		"""Get validation data in form of list of lists where each list contains data for one cml."""

		return self.split_into_list_of_lists(validation=True)

	def get_test_data(self) -> typing.List[typing.List[pd.DataFrame]]:
		"""Get test data in form of list of lists where each list contains data for one cml."""

		return self.split_into_list_of_lists(validation=True)

	def get_static_data(self) -> pd.DataFrame:
		"""Get static data for cmls in self.cmls_to_use."""

		return self.static_data.loc[[int(x) for x in self.cmls_to_use], self.static_data_columns]

	def load_all_data(self) -> None:
		"""Load dynamic data."""

		self.all_data = pd.read_csv(data_paths.ALL_DATA)
		self.all_data = self.convert_to_time_series(self.all_data)

	def drop_not_relevant_cmls(self) -> None:
		"""Drop cmls from dynamic data that are not in self.cmls."""

		retain_columns = self.rg_data_and_reference.copy()
		retain_columns.extend(self.cmls_to_use)
		self.all_data = self.all_data.loc[:, retain_columns]

	def drop_not_relevant_references(self) -> None:
		"""Drop not relevant references from self.all_data."""

		retain_columns = self.cmls_to_use.copy()
		for _, ref in self.cml_reference.items():
			rg, ref = ref
			if ref not in retain_columns:
				retain_columns.append(ref)
				retain_columns.append(rg)
		self.all_data = self.all_data.loc[:, retain_columns]

	def split_into_list_of_lists(self, training: bool = None, validation: bool = None, test: bool = None) -> typing.List[typing.List[pd.DataFrame]]:
		"""Split a dataframe into list od lists where each list contains one cml with its refs."""

		result = []
		for cml, refs in self.cml_reference.items():
			rain_ref, classification_ref = refs
			tmp = self.all_data.loc[:, [cml, rain_ref, classification_ref]]
			tmp.rename({rain_ref: "rain_reference", classification_ref: "classification_reference"}, inplace=True, axis=1)
			tmp = tmp.loc[tmp[cml].notna(), :]

			# use only part of the data based on input params
			size = tmp.shape[0]
			if training:
				tmp = tmp.iloc[: int(self.training_data_size*size), :]
			elif validation:
				tmp = tmp.iloc[int(self.training_data_size*size): int(self.validation_data_size*size), :]
			elif test:
				tmp = tmp.iloc[int(self.validation_data_size*size):, :]
			else:
				print(f"Neither training, validation nor test parameter is set. Correct code as you will not get correct result!!!")
				print("Exiting program")
				exit(1)
			result.append([tmp])
		return result
