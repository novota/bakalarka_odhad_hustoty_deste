from abc import ABC, abstractmethod
import data_paths
import datetime
import pandas as pd
import typing


class DataBaseInterface(ABC):

	cmls_to_use: typing.List[str]
	cml_reference: dict

	@abstractmethod
	def __init__(self, cmls: typing.List[str],  training_data_size: float, validation_data_size: float):
		pass

	@abstractmethod
	def get_training_data(self):
		"""Get training data in form of list of lists where each list contains data for one cml."""

		pass

	@abstractmethod
	def get_validation_data(self):
		"""Get validation data in form of list of lists where each list contains data for one cml."""

		pass

	@abstractmethod
	def get_test_data(self):
		"""Get test data in form of list of lists where each list contains data for one cml."""

		pass

	@abstractmethod
	def get_static_data(self):
		"""Get static data for cmls in self.cmls_to_use."""

		pass


class DataBase(DataBaseInterface):

	cmls_to_use: typing.List[str]
	cml_reference: dict
	training_data_size: float
	validation_data_size: float
	static_data: pd.DataFrame
	static_data_columns: list = ['frequency', 'polarisation_V', 'polarisation_H', 'length', 'reference_distance_km']

	def __init__(self, cmls: typing.List[str],  training_data_size: float, validation_data_size: float, reference: typing.List[typing.Tuple[str, str]] = None):
		self.cmls_to_use = cmls
		self.training_data_size = training_data_size
		self.validation_data_size = validation_data_size

		self.load_static_data()
		self.find_reference(reference=reference)

	@abstractmethod
	def get_training_data(self) -> typing.List[typing.List[pd.DataFrame]]:
		"""Get training data in form of list of lists where each list contains data for one cml."""

		pass

	@abstractmethod
	def get_validation_data(self) -> typing.List[typing.List[pd.DataFrame]]:
		"""Get validation data in form of list of lists where each list contains data for one cml."""

		pass

	@abstractmethod
	def get_test_data(self) -> typing.List[typing.List[pd.DataFrame]]:
		"""Get test data in form of list of lists where each list contains data for one cml."""

		pass

	@abstractmethod
	def get_static_data(self) -> pd.DataFrame:
		"""Get static data for cmls in self.cmls_to_use."""

		pass

	@staticmethod
	def convert_to_time_series(df: pd.DataFrame) -> pd.DataFrame:
		"""Convert a dataframe with time column to a time series."""

		try:
			list(df.columns).index("time")
		except ValueError:
			print("there is no column called time, cannot convert to time series dataframe")
			return df
		df.time = [datetime.datetime.fromisoformat(t) for t in df.time]
		return df.set_index("time")

	def load_static_data(self) -> None:
		"""Load static data"""

		self.static_data = pd.read_csv(data_paths.STATIC_DATA)
		self.static_data.rename({"Unnamed: 0": "cml"}, inplace=True, axis=1)
		self.static_data.set_index("cml", inplace=True)

	def get_reference_for_cmls(self, cmls: typing.List[str]) -> dict:
		"""Find reference for each cml in cmls based on the static data."""

		result = {}
		for cml in cmls:
			rg = self.static_data.loc[self.static_data.index == 55, "reference"].values[0]
			ref = f"{rg}_GT"
			result[cml] = self.static_data.loc[self.static_data.index == 55, "reference"].values[0], ref
		return result

	def find_reference(self, reference) -> None:
		"""Create a dictionary with cml keys and their reference as values."""

		if reference is None:
			self.cml_reference = self.get_reference_for_cmls(self.cmls_to_use)
		else:
			for idx, ref in enumerate(reference):
				rg, ref = ref
				self.cml_reference[self.cmls_to_use[idx]] = rg, ref

