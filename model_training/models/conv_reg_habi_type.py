### CONV_3 model based on habi et al ###

import mse_loss
import torch
import torch.utils.data
import torch.nn as nn
import typing
import pandas as pd

import data
import interface


def output_length(in_length, kernel, stride, padding, nr_layers):
	if nr_layers == 0:
		return in_length

	tmp = (in_length + 2 * padding - kernel) / stride + 1
	nr_layers -= 1
	return output_length(tmp, kernel, stride, padding, nr_layers)


class ConvNetRegHabi(nn.Module):
	def __init__(self, affine=True):
		super(ConvNetRegHabi, self).__init__()

		self.model_type = "regression"

		self.loss_function = mse_loss.MSEScaledLoss(delta_s=0.95, delta_r=5)
		self.in_length = 1440
		self.kernel = 5
		self.pooling_kernel = 2
		self.stride = 1
		self.pooling_stride = 2
		self.padding = 2
		self.out_channels = 24
		self.last_n_vals = 10
		self.bias = False
		self.affine = affine

		self.static_data_length = 5
		self.output_length = int(output_length(self.in_length, self.pooling_kernel, self.pooling_stride, padding=0, nr_layers=8))
		self.linear_input_size = self.output_length * self.out_channels + self.static_data_length + 850

		self.linear_static = nn.Sequential(
			nn.Linear(in_features=self.static_data_length, out_features=self.static_data_length, bias=True),
			nn.ReLU(),
			nn.Linear(in_features=self.static_data_length, out_features=self.static_data_length, bias=True),
			nn.ReLU(),
			nn.Linear(in_features=self.static_data_length, out_features=self.static_data_length, bias=True)
		)

		self.norm = nn.BatchNorm1d(num_features=1, affine=self.affine)

		self.conv_1 = nn.Sequential(
			nn.Conv1d(in_channels=1, out_channels=3, kernel_size=(self.kernel,), stride=(self.stride,), padding=self.padding, bias=self.bias),
			nn.BatchNorm1d(num_features=3, affine=self.affine),
			nn.LeakyReLU(0.1),
			nn.MaxPool1d(kernel_size=self.pooling_kernel, stride=self.pooling_stride, padding=0))

		self.conv_2 = nn.Sequential(
			nn.Conv1d(in_channels=3, out_channels=6, kernel_size=(self.kernel,), stride=(self.stride,), padding=self.padding, bias=self.bias),
			nn.BatchNorm1d(num_features=6, affine=self.affine),
			nn.LeakyReLU(0.1),
			nn.MaxPool1d(kernel_size=self.pooling_kernel, stride=self.pooling_stride, padding=0))

		self.conv_3 = nn.Sequential(
			nn.Conv1d(in_channels=6, out_channels=9, kernel_size=(self.kernel,), stride=(self.stride,), padding=self.padding, bias=self.bias),
			nn.BatchNorm1d(num_features=9, affine=self.affine),
			nn.LeakyReLU(0.1),
			nn.MaxPool1d(kernel_size=self.pooling_kernel, stride=self.pooling_stride, padding=0))

		self.conv_4 = nn.Sequential(
			nn.Conv1d(in_channels=9, out_channels=12, kernel_size=(self.kernel,), stride=(self.stride,), padding=self.padding, bias=self.bias),
			nn.BatchNorm1d(num_features=12, affine=self.affine),
			nn.LeakyReLU(0.1),
			nn.MaxPool1d(kernel_size=self.pooling_kernel, stride=self.pooling_stride, padding=0))

		self.conv_5 = nn.Sequential(
			nn.Conv1d(in_channels=12, out_channels=15, kernel_size=(self.kernel,), stride=(self.stride,), padding=self.padding, bias=self.bias),
			nn.BatchNorm1d(num_features=15, affine=self.affine),
			nn.LeakyReLU(0.1),
			nn.MaxPool1d(kernel_size=self.pooling_kernel, stride=self.pooling_stride, padding=0))

		self.conv_6 = nn.Sequential(
			nn.Conv1d(in_channels=15, out_channels=18, kernel_size=(self.kernel,), stride=(self.stride,), padding=self.padding, bias=self.bias),
			nn.BatchNorm1d(num_features=18, affine=self.affine),
			nn.LeakyReLU(0.1),
			nn.MaxPool1d(kernel_size=self.pooling_kernel, stride=self.pooling_stride, padding=0))

		self.conv_7 = nn.Sequential(
			nn.Conv1d(in_channels=18, out_channels=21, kernel_size=(self.kernel,), stride=(self.stride,), padding=self.padding, bias=self.bias),
			nn.BatchNorm1d(num_features=21, affine=self.affine),
			nn.LeakyReLU(0.1),
			nn.MaxPool1d(kernel_size=self.pooling_kernel, stride=self.pooling_stride, padding=0))

		self.conv_8 = nn.Sequential(
			nn.Conv1d(in_channels=21, out_channels=24, kernel_size=(self.kernel,), stride=(self.stride,), padding=self.padding, bias=self.bias),
			nn.BatchNorm1d(num_features=24, affine=self.affine),
			nn.LeakyReLU(0.1),
			nn.MaxPool1d(kernel_size=self.pooling_kernel, stride=self.pooling_stride, padding=0))

		self.linear = nn.Sequential(
			nn.Linear(self.linear_input_size, self.linear_input_size, bias=self.bias),
			nn.LeakyReLU(0.1),
			nn.Linear(self.linear_input_size, self.linear_input_size, bias=self.bias),
			nn.LeakyReLU(0.1),
			nn.Linear(self.linear_input_size, 1, bias=self.bias))
		self.softmax = nn.Softmax(dim=1)
		self.relu = nn.ReLU()

	def forward(self, batch, device):
		x, static_data = self.get_data_from_batch(batch, device=device)

		x_static = self.linear_static(static_data)
		x = self.norm(x)

		# get last 10 values
		skip_1 = x[:, :, -self.last_n_vals:].reshape(x.shape[0], x.shape[1] * self.last_n_vals)
		x = self.conv_1(x)

		# get last 10 values
		skip_2 = x[:, :, -self.last_n_vals:].reshape(x.shape[0], x.shape[1] * self.last_n_vals)
		x = self.conv_2(x)

		# get last 10 values
		skip_3 = x[:, :, -self.last_n_vals:].reshape(x.shape[0], x.shape[1] * self.last_n_vals)
		x = self.conv_3(x)

		# get last 10 values
		skip_4 = x[:, :, -self.last_n_vals:].reshape(x.shape[0], x.shape[1] * self.last_n_vals)
		x = self.conv_4(x)

		# get last 10 values
		skip_5 = x[:, :, -self.last_n_vals:].reshape(x.shape[0], x.shape[1] * self.last_n_vals)
		x = self.conv_5(x)

		# get last 10 values
		skip_6 = x[:, :, -self.last_n_vals:].reshape(x.shape[0], x.shape[1] * self.last_n_vals)
		x = self.conv_6(x)

		# get last 10 values
		skip_7 = x[:, :, -self.last_n_vals:].reshape(x.shape[0], x.shape[1] * self.last_n_vals)
		x = self.conv_7(x)

		# get last 10 values
		skip_8 = x[:, :, -self.last_n_vals:].reshape(x.shape[0], x.shape[1] * self.last_n_vals)
		x = self.conv_8(x)

		# reshape for fully connected layer
		x = x.view(x.shape[0], self.output_length * self.out_channels)
		# add skip connections
		x = torch.cat((x, skip_1, skip_2, skip_3, skip_4, skip_5, skip_6, skip_7, skip_8, x_static), dim=1)
		x = self.linear(x)
		return x

	@staticmethod
	def get_data_from_batch(batch, device):
		"""Get relevant data from a batch"""

		x = batch["cml_data"].to(device)
		static_data = batch["static_data"].to(device)
		return x, static_data

	@staticmethod
	def get_reference(batch):
		"""return reference retrieved from batch"""

		return batch["rain_reference"]

	def compute_loss(self, predictions, reference):
		"""Compute loss."""

		return self.loss_function(predictions, reference)

	def create_data_loader(self, data_list: typing.List[pd.DataFrame], batch_size: int, static_data: pd.DataFrame = None) -> typing.List[torch.utils.data.DataLoader]:
		"""Model knows what data it needs. Create a data loaders that match the needs of this model."""

		data_time = [data.TimeseriesDataset(df.iloc[:, 0], self.in_length, rain_reference=df.loc[:, "rain_reference"], static_data=static_data.loc[int(df.columns[0])].values.flatten().tolist()) for df in data_list if df.shape[0] >= self.in_length]
		return [torch.utils.data.DataLoader(data_timeseries, batch_size=batch_size, shuffle=False) for data_timeseries in data_time]


def get_model():
	return ConvNetRegHabi()
