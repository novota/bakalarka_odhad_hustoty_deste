
import pathlib

import pandas as pd
import torch


def clear_directory(directory: pathlib.Path) -> None:
	"""Delete all files within the directory. After each model training everything is stored in there and it must be moved and
	processed by another program before the next run starts."""

	files = [file for file in directory.glob("*.*")]

	for file in files:
		file.unlink()


def save_data_frame(df: pd.DataFrame, target_dir: pathlib.Path):
	"""Save data frame into a directory."""
	pass


def create_list_from_tensor(input_tensor: torch.Tensor):
	"""Convert torch tensor living on gpu to a list."""

	return [x.cpu().numpy().sum() for x in input_tensor]


def get_file_name_form_path(path: pathlib.Path) -> str:
	"""Get file nam from file path."""

	return str(path.relative_to(path.parent))
	
