import pandas as pd
import random
import torch
import torch.nn as nn
import torch.utils.data


class TimeseriesDataset(torch.utils.data.Dataset):
	def __init__(self, cml_data, seq_len: int, classification_reference=None, rain_reference=None, static_data=None):

		self.cml_data = torch.Tensor(cml_data.values)

		self.classification_reference = None if classification_reference is None else torch.Tensor(classification_reference.values).long()
		self.rain_intensity_reference = None if rain_reference is None else torch.Tensor(rain_reference.values)
		self.static = None if static_data is None else torch.Tensor(static_data)

		self.index = cml_data.index
		self.seq_len = seq_len

	def __getitem__(self, index):
		"""
		:param index: nbr of the data sample to be taken from the data loader
		:return: input data to the neural network
		"""
		data = self.cml_data[index:index+self.seq_len].unsqueeze(0)
		indexes = self.index[index+self.seq_len-1].timestamp()
		# add quantiles to the data
		# quantiles = torch.Tensor([data.min(), data.max(), data.std(), data.mean()])
		# data = torch.cat((quantiles, data))

		if random.random() > 0.5:
			data = data * random.randint(1, 3) * random.random()

		result = {"cml_data": data, "static_data": self.static, "indexes": indexes}
		if self.classification_reference is not None:
			result["classification_reference"] = self.classification_reference[index+self.seq_len-1]
		else:
			result["rain_reference"] = self.rain_intensity_reference[index+self.seq_len-1]

		return result

	def __len__(self):
		"""
		:return: maximal index
		"""

		return self.cml_data.__len__() - self.seq_len + 1
