import pandas as pd

import data_paths
import interface
import math
import numpy as np
import pathlib
import random
import torch
import torch.utils.data
import torch.nn as nn
import typing

from helper_functions import get_file_name_form_path


class Training:
	"""Training class."""

	optimizer: torch.optim
	loss_classification: nn.Module
	loss_regression: nn.Module
	data: interface.DataBaseInterface

	batch_size: int
	epochs: int
	lr: typing.List[float]
	weights: typing.List[typing.Tuple[float, float]]
	tune_hyperparams: bool

	# training variables
	cml_name: str = ""
	trn_loss: typing.List[float]
	val_loss: typing.List[float]
	predictions_list: typing.List[any]
	reference_list: typing.List[any]
	index_list: typing.List[any]

	def __init__(self, get_model_func, data: interface.DataBaseInterface, batch_size: int, epochs: int):
		"""Initialize training class."""

		self.data = data
		self.get_model_func = get_model_func

		self.batch_size = batch_size
		self.epochs = epochs

	def run_one_cml_at_time_training(self, lr: float, device):
		"""Train different models for each cml."""

		model_type = ""
		for idx, cml in enumerate(self.data.cmls_to_use):

			print(f"\ntraining cml: {cml}")
			# save current cml name
			self.cml_name = cml
			# get new model
			model = self.get_model_func()
			model.to(device)
			model_type = model.model_type

			# optimizer
			optimizer = torch.optim.Adam(model.parameters(), lr=lr)

			# data loaders
			training = model.create_data_loader(self.data.get_training_data()[idx], self.batch_size, self.data.get_static_data())
			validation = model.create_data_loader(self.data.get_validation_data()[idx], self.batch_size, self.data.get_static_data())
			test = model.create_data_loader(self.data.get_test_data()[idx], self.batch_size, self.data.get_static_data())
			data_loaders = (training, validation, test)

			try:
				self.train_model(model=model, data_loaders=data_loaders, device=device, optimizer=optimizer)
			except KeyboardInterrupt:
				print(f"training interrupted by user, saving end of training status flag")

		# when finished, create the status file END.txt which signalizes end of training for result analyzer
		# save "regression" or "classification" in the END.txt file so that the result analyzer knows which type of training it was
		self.set_end_of_training_status(model_type)

	def run_all_cmls_at_once_training(self, lr: float, device):
		"""Train model on all cmls."""

		# get new model
		model = self.get_model_func()
		model.to(device)

		self.cml_name = "all"

		# optimizer
		optimizer = torch.optim.Adam(model.parameters(), lr=lr)

		# data loaders
		training = model.create_data_loader(
			self.flatten_list(self.data.get_training_data()),
			self.batch_size,
			self.data.get_static_data())
		validation = model.create_data_loader(
			self.flatten_list(self.data.get_validation_data()),
			self.batch_size,
			self.data.get_static_data())
		test = model.create_data_loader(
			self.flatten_list(self.data.get_test_data()),
			self.batch_size,
			self.data.get_static_data())
		data_loaders = (training, validation, test)

		try:
			self.train_model(model=model, data_loaders=data_loaders, device=device, optimizer=optimizer)
		except KeyboardInterrupt:
			print(f"training interrupted, setting end of training status")

		# when finished, create the status file END.txt which signalizes end of training for result analyzer
		# save "regression" or "classification" in the END.txt file so that the result analyzer knows which type of training it was
		self.set_end_of_training_status(model.model_type)

	def tune_hyper_parameters(self, model: torch.nn.Module, data_loaders: tuple, device, optimizer):
		pass

	def train_model(self, model: torch.nn.Module, data_loaders: tuple, device, optimizer: torch.optim):
		"""Run all epochs of training."""

		training_data, validation_data, test_data = data_loaders
		best_epoch_loss = 1_000_000_000
		self.trn_loss = []
		self.val_loss = []
		
		try:
			for epoch in range(self.epochs):
				print(f"epoch: {epoch} / {self.epochs - 1}")
				model.train()
				self.train_epoch(model=model, data_loaders=training_data, device=device, optimizer=optimizer)
				model.eval()
				epoch_loss = self.evaluate_epoch(model=model, data_loaders=validation_data, device=device, validation=True)
				if epoch_loss < best_epoch_loss:
					best_epoch_loss = epoch_loss
					self.evaluate_epoch(model=model, data_loaders=test_data, device=device, validation=False)
					# save predictions for respective best validation loss
					self.save_predictions()
		except KeyboardInterrupt:
			print(f"training interrupted. saving predicitons and training progress")
		# save training and validation loss
		self.save_training_progress()

	def evaluate_epoch(self, model: torch.nn.Module, data_loaders: typing.List[torch.utils.data.DataLoader], device: torch.device, validation: bool) -> float:
		"""Evaluate performance of a model."""

		epoch_loss = 0
		if not validation:
			# clear lists
			self.init_lists()

		batches = self.get_batches_shuffled(data_loaders=data_loaders)

		for batch in batches:
			with torch.no_grad():
				predictions = model.forward(batch, device)
				reference = model.get_reference(batch).to(device)

				# check if there is any non-nan reference
				if reference.isnan().all():
					continue

				predictions, reference = self.ged_rid_of_nan_references(predictions, reference)

				loss = model.loss_function(predictions, reference)

				epoch_loss += loss.item()

				# save predicitons only for the test data
				if not validation:
					# save new test results for the best validation score
					if model.model_type == "classification":
						self.predictions_list.append(predictions)
					elif model.model_type == "regression":
						self.predictions_list.append(predictions.squeeze(dim=1))
					else:
						print(f"unknown model type: {model.model_type}.\nExiting program")
						exit(1)
					self.reference_list.append(reference)
					self.index_list.append(batch["indexes"])

		# report epoch results
		self.report_epoch_results(phase="validation" if validation else "testing", epoch_loss=epoch_loss)
		# when using validation data, save epoch loss, when using test data, dont save loss
		if validation:
			self.val_loss.append(epoch_loss)
		else:
			# convert lists storing results to lists of floats
			self.create_list_of_floats()
			if model.model_type == "classification":
				self.print_f1_score()

		return epoch_loss

	def train_epoch(self, model: torch.nn.Module, data_loaders: typing.List[torch.utils.data.DataLoader], device: torch.device, optimizer: torch.optim) -> None:

		epoch_loss = 0
		counter = 0

		batches = self.get_batches_shuffled(data_loaders=data_loaders)

		for batch in batches:
			predictions = model.forward(batch, device)
			reference = model.get_reference(batch).to(device)

			# check if there is any non-nan reference
			if reference.isnan().all():
				continue

			predictions, reference = self.ged_rid_of_nan_references(predictions, reference)
			loss = model.loss_function(predictions, reference)
			epoch_loss += loss.item()
			loss.backward()
			optimizer.step()
			optimizer.zero_grad()
			counter += 1

		# report epoch results
		self.report_epoch_results(phase="training", epoch_loss=epoch_loss)
		# save epoch loss to the list of all loses.
		self.trn_loss.append(epoch_loss)

	def create_list_of_floats(self):
		"""Convert class variable lists of lists of tensors to lists of floats [predictions_list, reference_list, index_list]."""

		if len(self.predictions_list) == 0:
			return
		# concatenate into single list
		self.predictions_list = torch.cat(self.predictions_list)
		self.reference_list = torch.cat(self.reference_list)
		self.index_list = torch.cat(self.index_list)

		# convert to lists of floats
		self.predictions_list = self.convert_tensors_to_floats(self.predictions_list)
		self.reference_list = self.convert_tensors_to_floats(self.reference_list)
		self.index_list = self.convert_tensors_to_floats(self.index_list)

	def init_lists(self):
		"""Init predictions_list, reference_list, index_list."""

		self.predictions_list = []
		self.reference_list = []
		self.index_list = []

	def report_epoch_results(self, phase: str, epoch_loss: float):
		"""Report result after one epoch."""

		print(f"{phase} loss is {epoch_loss}")

	def save_predictions(self):
		"""Save predictions, references and indexes."""

		print(f"saving predictions to: {data_paths.RESULTS_DIR}/{self.cml_name}\n")
		no_rain = [x[0] for x in self.predictions_list]
		rain = [x[1] for x in self.predictions_list]
		df = pd.DataFrame({"no_rain": no_rain, "rain": rain, "reference": self.reference_list, "index": self.index_list})
		df.to_csv(data_paths.RESULTS_DIR / f"{self.cml_name}.csv")
	
	def save_training_progress(self):

		print(f"saving training progress of cml: {self.cml_name}")
		df = pd.DataFrame({"training_loss": self.trn_loss, "validation_loss": self.val_loss})
		df.to_csv(data_paths.RESULTS_DIR / f"{self.cml_name}_training_progress.csv")

	def print_f1_score(self):
		"""Calculate and print f1-score"""
		
		if len(self.predictions_list) == 0:
			print(f"no predictions")
			return
		predictions = [np.argmax(x) for x in self.predictions_list]
		true_positive = ((predictions == 1) & (self.reference_list == 1)).sum()
		false_positive = ((predictions == 1) & (self.reference_list == 0)).sum()
		false_negative = ((predictions == 0) & (self.reference_list == 1)).sum()

		if true_positive == 0:
			print(f"f1-score is 0")
			return

		precision = true_positive / (true_positive + false_positive)
		recall = true_positive / (true_positive + false_negative)
		f1_score = 2 * precision * recall / (precision + recall)
		print(f"f1-score is {f1_score}")

	@staticmethod
	def convert_tensors_to_floats(input_tensor: torch.Tensor):
		"""Convert list of tensors to numpy array."""

		return input_tensor.cpu().numpy()

	@staticmethod
	def set_end_of_training_status(model_type: str):
		"""Create a END.txt file containing either 'regression' or 'classification' based on the model."""

		with open(data_paths.STATUS_DIR / "END.txt", "w") as f:
			f.write(model_type + "\n")
			f.write(get_file_name_form_path(data_paths.REFERENCE_DATA))

	@staticmethod
	def flatten_list(a_list):
		"""Flatten a list of lists."""

		return [item for sublist in a_list for item in sublist]

	@staticmethod
	def get_batches_shuffled(data_loaders):

		res = []
		for dl in data_loaders:
			for batch in dl:
				res.append(batch)
		random.shuffle(res)
		return res

	@staticmethod
	def ged_rid_of_nan_references(predictions, reference):

		mask = ~reference.isnan()
		return predictions[mask], reference[mask]
