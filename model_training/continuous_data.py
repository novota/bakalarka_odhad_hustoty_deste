import pathlib
import typing

import pandas as pd

import data_paths
import interface


class ContinuousData(interface.DataBase):

	training_data: typing.List
	validation_data: typing.List
	test_data: typing.List
	reference_data: pd.DataFrame
	seq_len: int

	def __init__(self, cmls: typing.List[str], training_data_size: float, validation_data_size: float, reference: typing.List[typing.Tuple[str, str]] = None, seq_len: int = None):
		"""Initialize class instance.

		:param cmls: Cml names that will be used for training
		:param training_data_size: the size of training data as a number in range [0, 1]
		:param validation_data_size: the size of validation data set as a number in range [training_data_size, 1], the rest will be used as test data
		:param reference: List containing tuple (rain reference, classification reference. the order is important as 1st tuple corresponds to the 1st cml in cmls
		:param seq_len: minimal sequence length needed for training and predictions
		"""

		super(ContinuousData, self).__init__(cmls=cmls, training_data_size=training_data_size, validation_data_size=validation_data_size, reference=reference)
		self.seq_len = seq_len
		self.training_data = []
		self.validation_data = []
		self.test_data = []

		self.load_reference_data()
		self.create_data_sets()

	def get_training_data(self) -> typing.List[typing.List[pd.DataFrame]]:

		return self.training_data

	def get_validation_data(self) -> typing.List[typing.List[pd.DataFrame]]:

		return self.validation_data

	def get_test_data(self) -> typing.List[typing.List[pd.DataFrame]]:

		return self.validation_data

	def get_static_data(self) -> pd.DataFrame:

		return self.static_data.loc[[int(x) for x in self.cmls_to_use], self.static_data_columns]

	def create_data_sets(self) -> None:
		"""Populates the training, validation and test data with dynamic data and reference."""

		# get list of all folders for given cmls_to_use
		folders_with_dynamic_data = [data_paths.DYNAMIC_DATA_FOLDER / cml for cml in self.cmls_to_use]

		for idx, folder in enumerate(folders_with_dynamic_data):
			# get all *.csv files in this folder
			csvs = [file for file in folder.glob("*.csv")]

			# get list of dataframes for a given cml
			list_of_dataframes = [self.prepare_data_frame(x, idx) for x in csvs]

			# get total number of samples for current cml
			samples_count = sum([x.shape[0] - self.seq_len - 1 for x in list_of_dataframes if x.shape[0] >= self.seq_len])
			training, validation, test = self.split_dataframes(list_of_dataframes=list_of_dataframes, samples_count=samples_count)

			# append results to compliant list
			self.training_data.append(training)
			self.validation_data.append(validation)
			self.test_data.append(test)

	def prepare_data_frame(self, data_path: pathlib.Path, idx: int) -> pd.DataFrame:
		"""Load a dataframe and convert it to time series then add reference data."""

		# load dataframe
		tmp = pd.read_csv(data_path)
		# convert it to time series
		tmp = self.convert_to_time_series(tmp)
		# get current cml
		cml = self.cmls_to_use[idx]
		# add reference
		rg, ref = self.cml_reference[cml]
		tmp_ref = self.reference_data.loc[:, [rg, ref]]
		tmp = tmp.join(tmp_ref, how="inner")
		# rename columns
		tmp.rename({rg: "rain_reference", ref: "classification_reference"}, axis=1, inplace=True)
		return tmp

	def load_reference_data(self):
		"""Load reference data."""

		self.reference_data = pd.read_csv(data_paths.REFERENCE_DATA)
		self.reference_data = self.convert_to_time_series(self.reference_data)

	def split_dataframes(self, list_of_dataframes: list, samples_count: int) -> typing.Tuple[list, list, list]:
		"""Split list of data frames into training, validation and test part."""

		training_set = []
		validation_set = []
		test_set = []
		trn_counter = 0
		val_counter = 0
		test_counter = 0

		train_count = int(samples_count * self.training_data_size)
		validation_count = int(samples_count * (self.validation_data_size - self.training_data_size))
		test_count = int(samples_count * (1 - self.validation_data_size))
		train_count += (samples_count - (train_count + validation_count + test_count))  # make sure train + validation + test == samples count

		for df in list_of_dataframes:
			if trn_counter < train_count:
				trn_counter, val_counter = self.place_data(df, training_set, validation_set, train_count, trn_counter, val_counter)
			elif val_counter < validation_count:
				val_counter, test_counter = self.place_data(df, validation_set, test_set, validation_count, val_counter, test_counter)
			else:
				test_set.append(df)

		return training_set, validation_set, test_set

	def place_data(self, df: pd.DataFrame, first_set: list, second_set: list, maximal_count: int, first_counter: int, second_counter: int) -> typing.Tuple[int, int]:
		"""Place dataframe into first set or split it between first and second set if  current count + samples from df > first counter."""

		nr_samples = df.shape[0] - self.seq_len - 1
		# there is not enough data to create even one counter
		if nr_samples < 1:
			return first_counter, second_counter

		if first_counter + nr_samples <= maximal_count:
			first_set.append(df)
			first_counter += nr_samples
			return first_counter, second_counter

		# split data
		second_counter += self.split_data(data=df, nr_samples=(maximal_count - first_counter), list1=first_set, list2=second_set)
		return maximal_count, second_counter

	def split_data(self, data: pd.DataFrame, nr_samples: int, list1: list, list2: list) -> int:
		"""Split data into nr_samples size and the rest."""

		# if nr_samples is < self.seq_len, dont split it
		if (nr_samples < self.seq_len) or ((data.shape[0] - nr_samples) < self.seq_len):
			list1.append(data)
			return 0

		list1.append(data.iloc[: nr_samples, :])
		list2.append(data.iloc[nr_samples:, :])
		return data.shape[0] - nr_samples
